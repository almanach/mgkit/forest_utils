# $Id$
package Forest;

require 5.005;
use strict;
use warnings;

##use Data::Grove;
##our @ISA     = qw(Data::Grove);

our $VERSION = 0.20;

sub new {
  my $type = shift;
  my $self = ($#_ == 0) ? { %{ (shift) } } : { @_ };
  return bless $self, $type;
}

package Forest::Op;
# our @ISA = qw{Forest};

sub new {
  my $type = shift;
  my $self = ($#_ == 0) ? { %{ (shift) } } : { @_ };
  return bless $self, $type;
}

package Forest::Deriv;
#our @ISA = qw{Forest};

sub new {
  my $type = shift;
  my $self = ($#_ == 0) ? { %{ (shift) } } : { @_ };
  return bless $self, $type;
}

package Forest::Node;
#our @ISA = qw{Forest};

sub new {
  my $type = shift;
  my $self = ($#_ == 0) ? { %{ (shift) } } : { @_ };
  return bless $self, $type;
}

package Forest::Feature;
#our @ISA = qw{Forest};

sub new {
  my $type = shift;
  my $self = ($#_ == 0) ? { %{ (shift) } } : { @_ };
  return bless $self, $type;
}

package Forest::Comp;
#our @ISA = qw{Forest};

sub new {
  my $type = shift;
  my $self = ($#_ == 0) ? { %{ (shift) } } : { @_ };
  return bless $self, $type;
}


package Forest::Val;
#our @ISA = qw{Forest};


sub new {
  my $type = shift;
  my $self = ($#_ == 0) ? { %{ (shift) } } : { @_ };
  return bless $self, $type;
}

{ my %vals = ();
  sub new_uniq {
    my $type = shift;
    my $v = shift;
    return $vals{$v} ||= bless { val => $v }, $type;
  }
}

package Forest::Plus;
##our @ISA = qw{Forest};

{
  my $plus;
  sub new {
    return $plus ||= bless {}, 'Forest::Plus';
  }
}

package Forest::Minus;
##our @ISA = qw{Forest};

{
  my $minus;
  sub new {
    return $minus ||= bless {}, 'Forest::Minus';
  }
}

package Forest::Shared;
#our @ISA = qw{Forest};

sub new {
  my $type = shift;
  my $self = ($#_ == 0) ? { %{ (shift) } } : { @_ };
  return bless $self, $type;
}


package Forest::Hypertag;
our @ISA = qw{Forest};

1;
__END__

=head1 NAME

Forest - Perl Objects to represent TAG Derivation Forest

=head1 SYNOPSIS

  use Forest;

=head1 DESCRIPTION

=head2 EXPORT

None by default.

=head1 SEE ALSO

Forest::LP::Parser (3)
Forest::XML::Writer (3)
Forest::HTML::Writer (3)
perl(1).

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2011, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

