# $Id$
package Forest::RCG::Parser;

require 5.005;
use strict;
use warnings;
use Forest;

our $forest;
our %map;
our @sentence;

sub parse {
    local *IN = shift;
    $forest = Forest->new( {@_, 'op'=>{} } );
    $forest->{'nb'} = 0;
    my $sentence = $forest->{'sentence'};
    $sentence =~ s/^\s+//o;
    @sentence = split(/\s+/,$sentence);
    %map=();
    while (<IN>){
	next if (/^\s*$/);	# skip blank lines
	next if (/^\s*%/);		# skip the sentence
	my ($tree,$lit,$body) = /^\s*(\S+?)\s*:\s+(.*?)\s+-->\s+(.*)\s+\./;
	my $head = &analyze_litteral($lit);
	next if ($head == -1);	# axiom production or void adjunctions
	my $op = $forest->{'op'}{$head};
	my $deriv = Forest::Deriv->new( 'nodes' => {} );
	my %cat = ();
	my @ranges=();
	foreach my $lit_body ($body =~ /([[:alnum:]\[\]]+\(.*?\))/og) {
	    my $lit_info = &analyze_node_litteral($lit_body);
	    my $lit_nb = $lit_info->{'op'};
#	    print "Found body lit $lit_nb for $lit_body\n";
	    next if ($lit_nb == -1); # do not insert void adjunctions
	    my $lit_op = $forest->{'op'}{$lit_nb};
	    my $lit_cat = $lit_op->{'cat'};
	    my $lit_node = (defined $lit_info->{'node'}) ? $lit_info->{'node'} :  "$lit_cat#".$cat{$lit_cat}++;
	    push(@{$deriv->{'nodes'}{$lit_node}},$lit_nb);
	    push(@ranges,&add_ranges($lit_op->{'span'}));
	}
	my $anchor_start;
	push(@ranges,
	     [$op->{'span'}[0],$op->{'span'}[0]],
	     [$op->{'span'}[1],$op->{'span'}[1]]);
	if ($op->{'type'} eq 'adj') {
	    push(@ranges,[$op->{'span'}[2],$op->{'span'}[3]]);
	}
	@ranges = sort { $a->[1] <=> $b->[0] } @ranges;
#	map( (print "$_->[0] $_->[1],"), @ranges);
#	print "\n";
	my $current = shift @ranges;
	foreach my $tmp (@ranges) {
	    last if ($tmp->[0] != $current->[1]);
	    $current = $tmp;
	}
	$anchor_start = $current->[1];
#	print "Anchor span $anchor_start\n";
	push(@{$deriv->{'nodes'}{'<>'}},&add_pseudo_anchor($tree,$anchor_start));
	push(@{$op->{'derivations'}},$deriv);
    }
    return $forest;
}

sub add_ranges {
    my $span = shift;
    my @span = @{ $span };
    if (@span == 2) {
	return $span;
    } else {
	return ( [$span[0],$span[2]], [$span[3],$span[1]] );
    }
}

sub add_pseudo_anchor {
    my ($tree,$start) = @_;
    my $lit = "anchor$tree$start";
    if (defined $map{$lit}) {
	return $map{$lit};
    }
    my $head = $forest->{'nb'}++;
    $map{$lit} = $head;
    $forest->{'op'}{$head} = Forest::Op->new( 'id' => $head,
					      'type' => 'anchor',
					      'derivations' => [],
					      'span' => [$start,$start+1],
					      'lex' => $sentence[$start],
#					      'lex' => 'dummy',
					      'tree' => $tree,
					      );
    return $head;
}

sub analyze_node_litteral {
    my $nodelit = shift;
    if ($nodelit =~ /^\[(.*?)\]\s*(.*)$/) {
	return { 'node' =>  $1, 'op' => analyze_litteral($2) };
    } else {
	return { 'op' => analyze_litteral($nodelit) };
    }
}

sub analyze_litteral {
    my $lit = shift;
    my $nb;
    if (defined $map{$lit}) {
	return $map{$lit};
    }
#    print "Analyze lit $lit\n";
    if ($lit =~ /^(\w+)1\((\d+)\.{2}(\d+)\)/){	# substitution
	$nb=$forest->{'nb'}++;
	$map{$lit}=$nb;
	$forest->{'op'}{$nb} = Forest::Op->new( 'id'=> $nb,
						'type' => 'subst', 
						'cat' => $1, 
						'span' => [$2,$3],
						'derivations' => [],
						'top' => Forest::Feature->new( 'f' => {} )
						);
	return $nb;
    } elsif ($lit =~ /^(\w+)2\((\d+)\.{2}(\d+),\s+(\d+)\.{2}(\d+)\)/) {	# adjunction
	if ($2 == $3 && $4 == $5){
	    return -1;		# void adjunctions
	}
	$nb=$forest->{'nb'}++;
	$map{$lit}=$nb;
	$forest->{'op'}{$nb} = Forest::Op->new( 'id'=> $nb,
						'type' => 'adj', 
						'cat' => $1, 
						'span' => [$2,$5,$3,$4],
						'derivations' => [],
						'top' => Forest::Feature->new( 'f' => {} ),
						'bot' => Forest::Feature->new( 'f' => {} )
						);
	return $nb;
    } elsif ($lit =~ /^axiom\((\d+)\.\.(\d+)\)/) {
	return -1;
    }
}

1;

__END__

=head1 NAME

Forest::RCG::Parser - Parse the RCG representation of TAG Derivation forest

=head1 SYNOPSIS

  use Forest::RCG::Parser;

=head1 DESCRIPTION

=head2 EXPORT

None by default.

=head1 SEE ALSO

Forest (3)
perl(1).

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

