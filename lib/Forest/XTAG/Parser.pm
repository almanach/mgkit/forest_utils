# $Id$
package Forest::XTAG::Parser;

require 5.005;
use strict;
#use warnings;
use Forest;
#use XTAG::Deriv;

our $forest;
our %node;
our @start;
our %vars;

our $idpat;

sub parse {
    local *IN = shift;
    $forest = Forest->new( {@_, 'op'=>{} } );
    $forest->{'nb'} = 0;
    %node=();
    @start=();
    $idpat = qr/(?:0x)?([0-9a-f]{7})/;
    while (<IN>) {
	push(@start,$1), next if /^start: $idpat/;
	next unless /^$idpat/;
	my ($id,
	    $tree,
	    $lex,
	    $label,
	    $kind,
	    $status,
	    $rootlabel,
	    $adjlabel,
	    $leaf_and_features
	    ) = m{
		^$idpat:\s	                        # node id
		    (\S+?)		                # tree name
			\[(\w*).*?\]	                # lexical anchor
			    <(.+?)/(\w+)>               # node name / top or bot
				<->	                # separator
				    <(\w+)>             # node status 
					<(\w+)/(\w*)>   # root label / adj label 
					    (\S+)       # anchor and features
					    }xo;
	my @deriv = map( [ /$idpat/g ], ($' =~ /\[(.*?)\]/g));
	my $leaf = 1 if ($leaf_and_features =~ /^\[<>/o || $leaf_and_features =~ /^<>\[/o);
	my ($features) = ($leaf_and_features =~ /^<>\[(.*?)\]/o);
#	print "FEATURES $features\n";
	my @features = split(/;/,$features);
#	print "=> ",join(' *** ',@features)."\n";
	my %features = ();
	my $vars = 0;
	foreach my $feature (@features) {
	    my ($left,$right) = split(/=/,$feature);
#	    print "Handling feature $feature -> $left and $right\n";
	    $left = handle_path($left);
	    $right = handle_value($right);
	    my $r;
#	    print "\tvalue kind $right->{'kind'}\n";
	    if ($right->{'kind'} eq 'path') {
		$r = $features{$right->{'node'}}{$right->{'pos'}}{$right->{'comp'}};
		if (!$r) {
#		    print "\tsetting new var\n";
		    $r = { 'kind' => 'var',
			   'value' => $vars++
			   };
		    $features{$right->{'node'}}{$right->{'pos'}}{$right->{'comp'}} = $r;
		}
	    } else {
#		print "\treusing\n";
		$r = $right;
	    }
	    $features{$left->{'node'}}{$left->{'pos'}}{$left->{'comp'}} = $r;
	}
	$node{$id} = { 'tree' => $tree,
		       'lex' => $lex,
		       'label' => $label,
		       'kind' => $kind,
		       'status' => $status,
		       'root' => $rootlabel,
		       'adj' => $adjlabel,
		       'leaf' => $leaf,
		       'deriv' => [ @deriv ],
		       'features' => { %features }
		       };
#	&display($id);
#	&display_features({%features});
    }
#    print "START ID=$start\n\n";
    while (@start) {
	&op_handle(0,shift @start);
    }
    return $forest;
}

sub display_features {
    my $features = shift;
    foreach my $node (keys %{$features}) {
	foreach my $pos (keys %{$features->{$node}}) {
	    foreach my $comp (keys %{$features->{$node}{$pos}}) {
		print "\t$node.$pos<$comp> = ";
		my $value = $features->{$node}{$pos}{$comp};
		print "[$value->{'kind'} $value] ";
		$value = ($value->{'kind'} eq 'var') ? "#$value->{'value'}" : join(':',@{$value->{'value'}});
		print "$value\n";
	    }
	}
    }
}

sub handle_path {
    my $path = shift;
#    print "handling path $path\n";
    if ($path =~ /^(\w+)\.([bt]):<(\S*)>/) {
	return { 'kind' => 'path',
		 'node' => $1,
		 'pos' => $2,
		 'comp' => $3 ? $3 : 'empty' };
    } else {
	return 0;
    }
}

sub handle_value {
    my $val = shift;
#    print "handling value $val\n";
    my $path = handle_path($val);
    if ($path) {
	return $path;
    }
    return { 'kind' => 'value',
	     'value' => [ split(qr{/}ox,$val) ]
	     };
}

sub display {
    my $id = shift;
    my $node = $node{$id};
    print <<EOF ;
id=$id
      tree:  $node->{'tree'}
       lex:  $node->{'lex'}
     label:  $node->{'label'}
      kind:  $node->{'kind'}
    status:  $node->{'status'}
      root:  $node->{'root'}
       adj:  $node->{'adj'}
      leaf:  $node->{'leaf'}
EOF
    foreach my $deriv (@{$node->{'deriv'}}){
	print "\tderiv: @{$deriv}\n";
    }
	print "\n";
}

sub norm_derivation {
    my $deriv = shift;
    my $nderiv = {};
    my @deriv = @{$deriv};
    while (@deriv) {
	my $label = shift @deriv;
	my $node = shift @deriv;
	push(@{$nderiv->{$label}},$node);
    }
    return $nderiv;
}

sub op_handle {
    my $left = shift;
    my $id = shift;
    my @stack = @_;
    my $node = $node{$id};

    if (defined $node->{'op'}) {
	return $node->{'op'};
    }
    
    my ($cat) = ($node->{'label'} =~ /^([A-Za-z]+)/);

    # should only handle nodes with kind=top and status=*root
    # returns the reference of an op
    
    my @derivations = deriv_handle($left,$node,@stack);

    my $right;
    my @foot=();
    my $top = Forest::Feature->new( 'f' => {} );
    my $bot = Forest::Feature->new( 'f' => {} );

    foreach my $derivation (@derivations) {
#	print "Derivation ",$derivation->as_string(),"\n";
	my $tmp = $derivation->{'span'}[1];
	if ($right && $right != $tmp) {
	    print STDERR "Pb of positions in derivations\n";
	}
        $right = $tmp;
	if (defined $derivation->{'span'}[2]) {
	    @foot = ( $derivation->{'span'}[2],
		      $derivation->{'span'}[3]);
	}
	if (defined $derivation->{'features'}) {
	    %vars = ();
	    $top->fill_from_xtag($derivation->{'features'}{$derivation->{'root'}}{'b'});
	    if (defined $derivation->{'foot'}){
		$bot->fill_from_xtag($derivation->{'features'}{$derivation->{'foot'}}{'t'});
	    }
	    $top->clean_anonymous();
	    $bot->clean_anonymous();
	}
    }
    
    my $nb = $forest->{'nb'}++;

    # creation of the op element
    # default is subst op
    my $op = Forest::Op->new( 'id' => $nb,
			      'cat' => $cat,
			      'type' => 'subst',
			      'derivations' => [ map( Forest::Deriv->new( 'nodes' => 
									  &norm_derivation($_->{'derivation'})),
						      @derivations)
						 ],
			      'top' => $top,
			      'span' => [$left, $right,@foot]
			      );
    
    if ($node->{'status'} eq 'auxroot') {
	# root of aux tree
	$op->{'type'} = 'adj';
	$op->{'bot'} = $bot;
    } elsif ($node->{'leaf'}) {
	# anchor node
	# may have the features for a tree
	# we filter the features relative to the root and foot nodes
	$op->{'type'} = 'anchor';
	undef $op->{'top'};
	$op->{'tree'} = $node->{'tree'};
	if ($node->{'lex'} =~ /^PRO$/) {
	    $op->{'lex'} = '[PRO]';
	    $op->{'span'} = [$left,$left]; # empty PRO trace leaves
	} elsif ($node->{'lex'} =~ /^\w+$/) {
	    $op->{'lex'} = $node->{'lex'};
	    $op->{'span'} = [$left,$left+1];
	} else {
	    $op->{'lex'} = '[trace]';
	    $op->{'span'} = [$left,$left]; # empty trace leaves
	}
    }
    
    $forest->{'op'}{$nb} = $op;
    $node->{'op'} = $nb;

    return $nb;

}

sub Forest::Feature::clean_anonymous {
    my $self = shift;
    foreach my $f (keys %{$self->{'f'}}) {
	if (defined $self->{'f'}{$f}{'id'} && $vars{$self->{'f'}{$f}{'id'}} == 1) {
	    delete $self->{'f'}{$f};
	}
    }
}

sub Forest::Feature::fill_from_xtag {
    my $self = shift;
    my $features = shift;
    foreach my $f (keys %{$features}) {
	$self->{'f'}{$f} = feature_convert($f,$features->{$f});
    }
}

sub feature_convert {
    my $feature = shift;
    my $value = shift;
    my $type = $value->{'kind'};
#    print "Feature convert $feature $type\n";
    if ($type eq 'value') {
	return Forest::Comp->new( 'comp' => $feature,
				  'value' => [ map(value_convert($_),@{$value->{'value'}}) ]
				  );
    } else {			# var
	$vars{$value->{'value'}}++;
	return Forest::Comp->new( 'comp' => $feature,
				  'value' => [],
				  'id' => $value->{'value'}
				  );
    }
}

sub value_convert {
    shift;
#    print "Value convert $_\n";
    s/^\s*//o;
    s/\s*$//;
    return Forest::Plus->new() if /^\+$/;
    return Forest::Minus->new() if /^-$/;
    return Forest::Val->new( val => $_ );
}

sub bot_handle {
    my $left = shift;
    my $id = shift;
    my @stack = @_;
    my $node = $node{$id};
    my $status = $node->{'status'};

    if ($node->{'status'} eq 'auxfoot') {
	# foot node
	# may have the features for a tree
	# we filter the features relative to the root and foot nodes
	return map( XTAG::Deriv->new( 'span' => [ $left,$_->{'span'}[1],
						  $left,$_->{'span'}[1]], 
				      'features' => $node->{'features'},
				      'foot' => $node->{'label'},
				      'derivation' => [] ), 
		    bot_handle($left,@stack) ); # first comp of @stack is the adjnode
    }

    if ($node->{'leaf'} || $status eq 'initroot' || $status eq 'auxroot' ){
	# node giving an op (lexical, subst, adj)
	my $op = op_handle($left,$id,@stack);
	my $label = $node->{'label'};
	my %supp = ();
	if ($node->{'leaf'}) {
#	    $label = "$label<>"; # needed to avoid having same label twice in a derivation
				 # only occur when an adj takes place on an anchor
				 # however: pb with multiple adj on same node !
				 # should move to a more complex representation of derivations !
	    $supp{'features'} = $node->{'features'};
	    $supp{'anchor'} = $node->{'label'};
	    $supp{'root'} = $node->{'root'};
	}
	return ( XTAG::Deriv->simple($label,$op,$forest->{'op'}{$op}{'span'},%supp));
    }
    
    return deriv_handle($left,$node,@stack);

}

sub deriv_handle {
    my $left = shift;
    my $node = shift;
    my @stack = @_;

#    print "deriv handle node=$node->{'label'} left=$left deriv=",map(@{$_},@{$node->{'deriv'}}),"\n";

    # otherwise internal or subst node
    my @derivations = ();
    
    # we compute the derivation list
    # each derivation is a reference to a list of <node><opref> pairs
    foreach my $deriv (@{$node->{'deriv'}}) {
	my $status = $node{$deriv->[0]}->{'status'};
	if ($status eq 'initroot') {
	    # substitution node
	    my $op = op_handle($left,$deriv->[0],@stack);
	    push(@derivations,
	       XTAG::Deriv->simple( $node->{'label'},
				    $op,
				    $forest->{'op'}{$op}{'span'}) );
	} elsif ($status eq 'auxroot') {
	    # adj on internal node
	    push(@derivations, adj_handle($left,$node,$deriv,@stack));
	} elsif (@{$deriv} == 2 && $node{$deriv->[1]}->{'status'} eq 'auxroot') {
	    # in some case the adj deriv is in reverse order
	    push(@derivations, adj_handle($left,$node,[$deriv->[1],$deriv->[0]],@stack));
	} else {
	    # children of internal node
	    push(@derivations, seq_handle($left,[@{$deriv}],@stack));
	}
    }
    
    return @derivations;
}

sub adj_handle {
    my $left = shift;
    my $node = shift;
    my $deriv = shift;
    my @stack = @_;
    my $head = &op_handle($left,$deriv->[0],$deriv->[1],@stack);
    my $headspan = $forest->{'op'}{$head}{'span'};
#    print "Adj on node $node->{'label'} aux=$head left=$headspan->[2] bot=$deriv->[1]\n";
    my @bot = bot_handle($headspan->[2],$deriv->[1],@stack);
#    display($deriv->[1]);
#    print "\t=>bot ",map($_->as_string(),@bot),"\n";
    return map( $_->adjoin( $node->{'label'},
			    $head,
			    $headspan ),
		@bot
		);
}

sub seq_handle {
    my $left =shift;
    my $sequence = shift;
    my @stack = @_;
#    print "sequence handle @{$sequence} left=$left\n";
    if (!@{$sequence}) {
	return ( XTAG::Deriv->new( 'span' => [$left,$left], 
				   'derivation' => [] ) );
    } else {
	my @product = ();
	my @sequence = @{$sequence};
	foreach my $a (bot_handle($left,shift @sequence,@stack)) {
	    foreach my $b (seq_handle($a->{'span'}[1],\@sequence,@stack)) {
		my $c = $a->concat($b);
#		print "concat ",$a->as_string()," with ",$b->as_string()," => ", $c->as_string(),"\n";
		push(@product,$c);
	    }
	}
	return @product;
    }
}

package XTAG::Deriv;
##use Data::Grove;

##our @ISA     = qw(Data::Grove);
our $VERSION = '0.01';

sub new {
  my $type = shift;
  my $self = ($#_ == 0) ? { %{ (shift) } } : { @_ };
  return bless $self, $type;
}

sub XTAG::Deriv::as_string {
    my $self = shift;
    my $s = "<DERIV>{r=$self->{'root'} a=$self->{'anchor'} f=$self->{'foot'}}[@{$self->{'span'}}]{";
    my @derivation = @{$self->{'derivation'}};
    while (@derivation) {
	$s .= " ".(shift @derivation)."=>".(shift @derivation);
    }
    $s .= "}";
    return $s;
}

sub XTAG::Deriv::concat {
    my $head = shift;
    my $tail = shift;
    my $new = XTAG::Deriv->new( 'span' => [ @{$tail->{'span'}} ],
				'derivation' => [ @{$tail->{'derivation'}} ],
				'foot' => $tail->{'foot'}
				);
    $new->{'span'}[0]=$head->{'span'}[0];
    unshift(@{$new->{'derivation'}},@{$head->{'derivation'}});
    my @features = ();
    push(@features,%{$tail->{'features'}}) if (defined $tail->{'features'});
    push(@features,%{$head->{'features'}}) if (defined $head->{'features'});
    $new->{'features'} = { @features } if (@features);
    if (defined $tail->{'anchor'}) {
	$new->{'anchor'} = $tail->{'anchor'};
    } elsif (defined $head->{'anchor'}) {
	$new->{'anchor'} = $head->{'anchor'};
    }
    if (defined $tail->{'root'}) {
	$new->{'root'} = $tail->{'root'};
    } elsif (defined $head->{'root'}) {
	$new->{'root'} = $head->{'root'};
    }
    if (@{$tail->{'span'}}==4) {
	$new->{'foot'} = $tail->{'foot'};
    } elsif (@{$head->{'span'}}==4) {
	push(@{$new->{'span'}},$head->{'span'}[2],$head->{'span'}[3]);
	$new->{'foot'} = $head->{'foot'};
    }
    return $new;
}

sub XTAG::Deriv::simple {
    my $type = shift;
    my $label = shift;
    my $op = shift;
    my $span = shift;
#    print "Simple deriv $label $op $span\n";
    return XTAG::Deriv->new( 'span' => [@{$span}], 
			     'derivation' => [ $label => $op],
			     @_
			     );
}

sub XTAG::Deriv::adjoin {
    my $self = shift;
    my $label = shift;
    my $auxop = shift;
    my $auxspan = shift;
#    print "Adjoin deriv ", $self->as_string()," with [@{$auxspan}]{$label => $auxop} => ";
    push(@{$self->{'derivation'}},$label => $auxop);
    $self->{'span'}[0] = $auxspan->[0];
    $self->{'span'}[1] = $auxspan->[1];
#    print $self->as_string(),"\n";
    return $self;
}

1;

__END__

=head1 NAME

Forest::XTAG::Parser - Parse the representation of TAG Derivation
forest produced by Anoop Sarkar's XTAG parser.

=head1 SYNOPSIS

  use Forest::XTAG::Parser;

=head1 DESCRIPTION

=head2 EXPORT

None by default.

=head1 SEE ALSO

Forest (3)
perl(1).
Anoop Sarkar <anoop@linc.cis.upenn.edu>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

