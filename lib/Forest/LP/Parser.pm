# $Id$
package Forest::LP::Parser;

require 5.005;
use strict;
use warnings;
use Forest;
#use Bloom::Filter;

#use Data::Dumper;

#our $pattern = qr/(\w+)\s*(?:{(.*?)})?\(\s*(\d+)\s*,\s*(\d+)\s*\)/o;
our $pattern = qr/(\w+)\s*(?:\{(.*?)\})?\(\s*(\S+)\s*,\s*(\S+)\s*\)/o;
our $forest;
our %vars;
our %shared=();
our %bodies=();

our %elements=();		# store xml representation of common values
our %cache=();

our %comp=();

our $type = 'lp';

our $config = { 'lp' => { 'adj' => \&lp_adj_parse,
			  'std' => \&lp_std_parse,
			  'anchor' => \&anchor_parse,
			  'lexical' => \&lexical_parse,
			  'struct' => \&struct_parse,
			  'epsilon' => \&epsilon_parse,
			},
		'xrcg' => { 'adj' => \&xrcg_adj_parse,
			    'std' => \&xrcg_std_parse,
			    'anchor' => \&anchor_parse,
			    'lexical' => \&lexical_parse
			    }
	    };

our $anonkey = "anon000000";
our $bodyid = 0;

#$::RD_TRACE=1;
$::RD_AUTOACTION = q{ $item[1] };

our $redid = "a000000";
our %reduce = ();

our $htid = "ht0000";

sub reduce_register_and_emit {
  my $s = shift;
  print STDERR "try reduce '$s'\n";
  return $reduce{$s} ||= do {
    ## inlined version of 'Forest::Shared'
    my $shared = $redid++;
    $shared{$shared} = bless { key => $shared,
			       shared => body_parse($s)
			     },'Forest::Shared';
    '#'.$shared;
  };
}

sub register_shared_and_emit {
  my $s = shift;
  my $shared = shift;
  $shared{$shared}  = bless { shared => body_parse($s),
			      key => $shared
			    },'Forest::Shared';
  return "#$shared";
}


sub production_parse {
  my $prod = shift;
  my ($head,$body) = ($prod =~ /(\d+)\s*<--\s*(.*)\s*$/);

  ## remove outermost parenthesis
  $body =~ s/^\((.*)\)\s*$/$1/o;

  ##  print "Body $body\n";
  ##  print "Body length=",length($body),"\n";

  while($body =~ /\(/) {
    ## 2011/07: no longer seem to be useful ! 
    # no sure this case may still arise
    ## progressively flatten the body by introducing shared/indirect subparts
    $body =~ s/\(\s*([^()]+?)\s*\)/reduce_register_and_emit($1)/oeg;
  }
  while ($body =~ /\#(a?\d+)\{/) {
    # seems more efficient to progressivly reduce (in a bottom-up way)
    # the internal shared parts #<i>{ ... }
    # replacing them by #<i>
    ## *** do not add /g to the next body rewriting
    ## it may induce use of a shared #x before its registration
    $body =~ s/\#(a?\d+)\{\s*([^{}]+?)\s*\}/register_shared_and_emit($2,$1)/oe;
  }

  #  print STDERR "Reduced $body\n";
  # need to clean cache bodies
  # we may have similar bodies correponding to distinct spans
  # => induce problems in the following phases
  %bodies = ();
  return ($head => body_parse($body));
}

sub body_parse {
  my $body = shift;
  #  print STDERR "body parse $body\n";
  my @stack = ();
  my $alternatives = [];
  my $current = {};
  my $shared;

 LOOP: {
    $body =~ /\G\s*$/ogc and last LOOP;
    $body =~ /\G\s+/ogc  and redo LOOP;
    $body =~ /\G\[(.+?)\](\d+)\s*/ogc and push(@{$current->{$1}},$2), redo LOOP;
    $body =~ /\G(\d+)\s*/ogc and push(@{$current->{void}},$1), redo LOOP;
    if ($body =~ /\G\|\s*/ogc) {
#      print STDERR "start alternative\n";
      push(@$alternatives,$current);
      $current = {};
      redo LOOP;
    }
#     if ($body =~ /\G\(\s*/ogc) {	
# #      print STDERR "open body\n";
#       unshift(@stack,[$current,$alternatives,$shared]);
#       $current = {};
#       $alternatives = [];
#       $shared = $anonkey++;
#       redo LOOP;
#     }
    if ($body =~ /\G\#(a?\d+)\{\s*/ogc) {
#      print STDERR "open shared body\n";
      unshift(@stack,[$current,$alternatives,$shared]);
      $current = {};
      $alternatives = [];
      $shared = $1;
      redo LOOP;
    }
    if ($body =~ /\G\#(a?\d+)\s*/ogc) {
      ## print STDERR "reusing shared body\n";
      my $tmp = $shared{$1} ||= bless {key => $1}, 'Forest::Shared';
      push(@{$current->{shared}},$tmp);
      redo LOOP;
    }
#    if ($body =~ /\G[)}]\s*/ogc) {
    if ($body =~ /\G\)\s*/ogc) {
#      print STDERR "closing (shared) body\n";
      (%$current || @$alternatives) and push(@$alternatives,$current);
      $current = sign_and_register($alternatives);
      #     print STDERR "built body id=>$current->{id} sign=>$current->{sign}\n";
      defined $shared or warn "WARNING: undefined shared key -- '$body'";
      my $tmp =  $Forest::LP::Parser::shared{$shared};
      defined $tmp and warn "WARNING: shared key $shared already defined -- '$body'";
      $tmp = $shared{$shared} ||= bless {key => $shared}, 'Forest::Shared';
      $tmp->{shared} = $current;
      my $n = @$current;
      my $nalt = @$alternatives;
#      print "Shared $shared $tmp $n $nalt\n";
      my $label = $shared;
      @stack or warn "*** ERROR Expected non empty stack";
      ($current,$alternatives,$shared) = @{ shift @stack };
      push(@{$current->{$label}},$tmp);
      redo LOOP;
    }
    warn "WARNING: should not be here -- '$body'\n";
  }
  @stack and warn "*** ERROR Expected empty stack -- '$body'";
  # don't forget to build the whole body
  (%$current || @$alternatives) and push(@$alternatives,$current);
  return sign_and_register($alternatives);
}

# $::RD_HINT=1;

sub fsargs_parse {
  my $fs = shift;
  my $simplify = shift || 0;
  my @stack;
  my $current = Forest::Feature->new( f=>{});	# current list of args
  my $comp;			# current feature value
  pos($fs)=0;
#  print STDERR "handling $fs\n";
 LOOP: {
#    print STDERR "next iteration pos=".pos($fs)."\n";
    $fs =~ /\G\s*$/ogc and last LOOP;
    $fs =~ /\G\s+/ogc and redo LOOP;
    if ($fs =~ /\G([\w\d-]+)\{/ogc) {
#      print STDERR "opening fs $1\n";
      unshift(@stack,[$current,$comp]);
      $current = Forest::Feature->new(type => $1, f => {} );
      $comp->{value} = [$current];
      redo LOOP;
    }
    if ($fs =~ /\G([\w\d-]+)\s*=>\s*/ogc) {
#      print STDERR "reading feature $1\n";
      $comp = Forest::Comp->new( comp=> $1, value => [] );
      $current->{f}{$1} = $comp;
      if ($fs =~ /\G<([A-Z_][\w\d]*)>\s*((?:::|,)?)\s*/ogc){
#	print STDERR "reading variable $1\n";
	$comp->{$2 eq "::" ? 'var' : 'id'} = $1;
      };
      redo LOOP;
    }
    if ($fs =~ /\G([\w\d-]+)\[(.*?)\]\s*(,\s*)?/ogc) {
#      print STDERR "reading fset value $1 [$2]\n";
      $comp->{value} = [map(fs_value($_),split(/\s*,\s*/,$2))];
      redo LOOP;
    }
    if ($simplify && ($fs =~ /\G-\s*(,\s*)?/ogc)) {
      $comp->{value} = [];
      redo LOOP;
    }
    if ($fs =~ /\G(\+|\-|[^,\[\]\{\}() ]+)\s*(,\s*)?/ogc) {
#      print STDERR "reading atomic value $1\n";
      $comp->{value} = [fs_value($1)];
      redo LOOP;
    }
    if ($fs =~ /\G\}\s*(,\s*)?/ogc) {
#      print STDERR "closing fs\n";
      my $val = $current;
      ($current,$comp) = @{ shift @stack };
      if ($simplify) {
	foreach my $f (keys %{$val->{f}}) {
	  delete $val->{f}{$f} unless (@{$val->{f}{$f}->{value}});
	}
	%{$val->{f}} or $comp->{value} = [];
      }
      redo LOOP;
    }
    my $pos = pos($fs);
    $fs =~ /\G(.*)/ogc;
    warn "WARNING: fsarg parser: should not arrive here at pos $pos: $fs\n\t at '$1'";
  }
  return $current;
}

sub fs_value {
  my $v = shift;
  my $builder = sub {
    ($v eq '-') and return Forest::Minus->new();
    ($v eq '+') and return Forest::Plus->new();
    return Forest::Val->new_uniq( $v );
  };
  return $elements{$v} ||= $builder->();
}

sub lp_adj_parse {
    my $nt = shift;
    if ($nt =~ /^$pattern\s*\*\s*$pattern/o) {
	return { 'cat' => $1, 
		 'top' => $2, 
		 'bot' => $6, 
		 'span' => [$3,$4,$7,$8] };
    } else {
	return 0;
    }
}

sub lp_std_parse {
    my $nt = shift;
    if ($nt =~ /^$pattern/o) {
	return { 'cat' => $1, 'top' => $2, 'span' => [$3,$4] };
    } else  {
	return 0;
    }
  }

sub struct_parse {
    my $nt = shift;
    if ($nt =~ m/^verbose!struct\(
                        \s*(.+?)\s*,   # Tree
			\s*(.+?)\s*
		 \)\s*$/ox) {
      ## New since July 2008: adding HyperTag to struct
      my ($tree,$ht) = ($1,$2);
      $ht =~ s/ht\{(.*)\}/$1/o;
##      print "HT $ht\n";
      return { 'tree' => $tree, 
	       'ht' => $ht
	     };
    } elsif ($nt =~ m/^verbose!struct\(
                        \s*(.+?)\s*   # Tree
		 \)/ox) {
      return { 'tree' => $1 };
    } else {
      return 0;
    }
  }

sub anchor_parse {
    my $nt = shift;
    if ($nt =~ m/^verbose!anchor\(
                        \s*(.+?)\s*,    # Token
                        \s*(\S+?)\s*,    # Left
                        \s*(\S+?)\s*,    # Right
                        \s*(.+?)\s*,    # Tree
                        \s*(\w+)\s*(?:\{(.*)\})?\s*,    # Cat{...}
                        \s*(?:\(\d+\s*:\s*)?
			\[
			     (.+?),
			     ((?:E[0-9.]+F\d+\|\S+)(?:\s+(?:E[0-9.]+F\d+\|\S+))*),
			     (.+?)
                           \]
                           (?:\))?\s*     # lemma,Lex,lemmaid
                        (,\s*tag_anchor\{.*\}\s*)?
                 \)/ox) {
      # new format: April 2011 (lemmaid)
      return { 'lex' => $8, token => $1, 
	       'lemma' => $7, lemmaid => $9, 
	       'cat' => $5, 'bot' => $6, 'span' => [$2,$3], 'tree' => $4 };
    } elsif ($nt =~ m/^verbose!anchor\(
                        \s*(.+?)\s*,    # Token
                        \s*(\S+?)\s*,    # Left
                        \s*(\S+?)\s*,    # Right
                        \s*(.+?)\s*,    # Tree
                        \s*(\w+)\s*(?:\{(.*)\})?\s*,    # Cat{...}
                        \s*\[(.+?),(.+?)\]\s*     # Lex
                        (,\s*tag_anchor\{.*\}\s*)?
                 \)/ox) {
      return { 'lex' => $8, token => $1, 'lemma' => $7, lemmaid => $7, 
	       'cat' => $5, 'bot' => $6, 'span' => [$2,$3], 'tree' => $4 };
    } elsif ($nt =~ m/^verbose!anchor\(
                        \s*(.+?)\s*,    # Token
                        \s*(\S+?)\s*,    # Left
                        \s*(\S+?)\s*,    # Right
                        \s*(.+?)\s*,    # Tree
                        \s*(\w+)\s*(?:\{(.*)\})?\s*,    # Cat{...}
                        \s*(.+?)\s*     # Lex
                 \)/ox) {
      # soon to be deprecated : May 11 2005
      return { 'lex' => $7, token => $1, 'cat' => $5, 'bot' => $6, 'span' => [$2,$3], 'tree' => $4 };
    } elsif ($nt =~ /^tag_anchor\(\s*(.+?)\s*,\s*(\S+?)\s*,\s*(\S+?)\s*,\s*(.+?)\s*\)/o) {
      # Deprecated
      return { 'lex' => $1, token => $1, 'span' => [$2,$3], 'tree' => $4 };
    } else {
      return 0;
    }
}

sub lexical_parse {
    my $nt = shift;
        if ($nt =~ m/^verbose!lexical\(
                         \s*\[\s*(.*?)\s*\]\s*,  ## Token
                         \s*(\S+?)\s*,            ## Left
                         \s*(\S+?)\s*,            ## Right
                         \s*(.+?)\s*,            ## Cat
                         \s*\[(.+?),((?:E[0-9.]+F\d+\|\S+)(?:\s+(?:E[0-9.]+F\d+\|\S+))*),(.+?)\]\s*     ## Lex LemmaId
                 \)/xo) {
      return { 'lex' => $6, token => $1, 'lemma' => $5, 
	       lemmaid => $7,
	       'bot' => '', 'span' => [$2,$3], cat => $4 };
    } elsif ($nt =~ m/^verbose!lexical\(
                         \s*\[\s*(.*?)\s*\]\s*,  ## Token
                         \s*(\S+?)\s*,            ## Left
                         \s*(\S+?)\s*,            ## Right
                         \s*(.+?)\s*,            ## Cat
                         \s*\[(.+?),(.+?)\]\s*             ## Lex
                 \)/xo) {
      return { 'lex' => $6, token => $1, 
	       'lemma' => $5, lemmaid => $5,
	       'bot' => '', 'span' => [$2,$3], cat => $4 };
    } elsif ($nt =~ m/^verbose!lexical\(
                         \s*\[\s*(.*?)\s*\]\s*,  ## Token
                         \s*(\S+?)\s*,            ## Left
                         \s*(\S+?)\s*,            ## Right
                         \s*(.+?)\s*,            ## Cat
                         \s*(.+?)\s*             ## Lex
                 \)/xo) {
      # soon to be deprecated : May 11 2005
      return { 'lex' => $5, token => $1, 'bot' => '', 'span' => [$2,$3], cat => $4 };
    }  elsif ($nt =~ m/^verbose!coanchor\(
		       \s*(.+?)\s*,    # Token
		       \s*(\S+?)\s*,    # Left
		       \s*(\S+?)\s*,    # Right
		       \s*(\w+)\s*(?:\{(.*)\})?\s*,    # Cat{...}
		       \s*\[(.+?),((?:E[0-9.]+F\d+\|\S+)(?:\s+(?:E[0-9.]+F\d+\|\S+))*),(.+?)\]\s*     # Lex lemmaid
                 \)/ox) {
      ## April 2011 (lemmaid)
      return { 'lex' => $7, token => $1, 'lemma' => $6, lemmaid => $8,
	       'cat' => $4, 'bot' => $5, 'span' => [$2,$3] };
    } elsif ($nt =~ m/^verbose!coanchor\(
		       \s*(.+?)\s*,    # Token
		       \s*(\S+?)\s*,    # Left
		       \s*(\S+?)\s*,    # Right
		       \s*(\w+)\s*(?:\{(.*)\})?\s*,    # Cat{...}
		       \s*\[(.+?),(.+?)\]\s*     # Lex
                 \)/ox) {
      return { 'lex' => $7, token => $1, 'lemma' => $6, lemmaid => $6,
	       'cat' => $4, 'bot' => $5, 'span' => [$2,$3] };
    } elsif ($nt =~ m/^verbose!coanchor\(
                        \s*(.+?)\s*,    # Token
                        \s*(\S+?)\s*,    # Left
                        \s*(\S+?)\s*,    # Right
                        \s*(\w+)\s*(?:\{(.*)\})?\s*,    # Cat{...}
                        \s*(.+?)\s*     # Lex
                 \)/ox) {
      # soon to be deprecated : May 11 2005
      return { 'lex' => $6, token => $1, 'cat' => $4, 'bot' => $5, 'span' => [$2,$3] };
    } elsif ($nt =~ /^tag_lexical\(\s*\[\s*(.*?)\s*\]\s*,\s*(\S+?)\s*,\s*(\S+?)\s*\)/o) {
      # Deprecated
      return { 'lex' => $1, token => $1, 'span' => [$2,$3] };
    } else {
      return 0;
    }
}

sub epsilon_parse {
  my $nt = shift;
  if ($nt =~ m/^verbose!epsilon\(
	       \s*(.+?)\s*,             ## Token
	       \s*(\S+?)\s*,            ## Left
	       \s*(\S+?)\s*,            ## Right
	       \s*\[(.+?),((?:E[0-9.]+F\d+\|\S+)(?:\s+(?:E[0-9.]+F\d+\|\S+))*),(.+?)\]\s*    ## Lex LemmaId
	       \)/xo) {
    ##    warn "FOUND EPSILON\n";
    return { 'lex' => $5, token => $1, 
	     'lemma' => $4, lemmaid=> $6,
	     'span' => [$2,$3] };
  } elsif ($nt =~ m/^verbose!epsilon\(
	       \s*(.+?)\s*,             ## Token
	       \s*(\S+?)\s*,            ## Left
	       \s*(\S+?)\s*,            ## Right
	       \s*\[(.+?),(.+?)\]\s*    ## Lex
	       \)/xo) {
    ##    warn "FOUND EPSILON\n";
    return { 'lex' => $5, token => $1, 
	     'lemma' => $4, lemmaid => $4,
	     'span' => [$2,$3] };
  } else {
    return 0;
  }
}

our $xrcg_pair = qr/\((\S+)\s*:\s*(\S+)\)/o;

sub xrcg_adj_parse {
    my $nt = shift;
    if ($nt =~ /^adj_(\w+)\(\w+(?:\{(.*?)\})?,\w+(?:\{(.*?)\})?\)\s*\($xrcg_pair,$xrcg_pair\)/o) {
	return { 'cat' => $1, 'top' => $2, 'bot' => $3, 'span' => [$4,$6,$5,$7] };
    } else {
	return 0;
    }
}

sub xrcg_std_parse {
    my $nt = shift;
    if ($nt =~ /^(\w+)(?:\{(.*?)\})?\s*\($xrcg_pair\)/o) {
	return { 'cat' => $1, 'top' => $2, 'span' => [$3,$4] };
    } else  {
	return 0;
    }
}

sub parse {
    my $handle = shift;
    my $options = shift || {};
    my $keep = $options->{keep};
    $forest = Forest->new( {@_, 
			    'op' =>{}, 
			    'hypertag' => {},
			    options => $options
			   });
    my $sid = $forest->{sid} ||= "E1";
    %shared = ();
    $anonkey = "anon000000";
    %bodies = ();
    %comp=();
    $bodyid = 0;
    $redid = "a000000";
    %reduce = ();
    $htid = "ht0000";

    $forest->{'nb'} = -1;

    while (<$handle>){
      if (/^\*+.*robust\s+mode/i) { # forest issued by partial robust parsing
	$forest->{mode} = 'robust';
	next;
      }
      if (/^\*+.*correction\s+mode/i) { # forest issued by partial robust parsing
	$forest->{mode} = 'corrected';
	next;
      }
      last if (/^Shared Forest/);
    }

##    print "starting reading forest\n";

    my $handler = $config->{$type};

    while (<$handle>){
##	print "LINE $.\n";
	next if (/^\s*$/);	# skip blank lines
	next if (/^Time\s+\d+/); # skip Time info
	next if (/^%ENDLOOP/);

	if (/^\*ANSWER\*/) {	# skip ANSWER production
	    $_=<$handle>;
	    next;
	}
##	s/\b([_A-Z][a-z_0-9]*)__(\d+)/$1$2/og;		# Remove trailing number for variables
	s/\b([_A-Z][a-z_0-9]*)__(\d+)/<$1$2>/og;	# Remove trailing number for variables
	my $nt=$_;
	my $production = <$handle>;
	chop $nt;
	chop $production;
	%vars=();
	my $info;
#    print "Dealing with $nt\n\t$production\n";
	my ($head,$derivations) = production_parse($production);
	if ($info=$handler->{'adj'}->($nt)) {
	    # Adjunctions
	    &var_set($info->{'top'});
	    &var_set($info->{'bot'});
	    $forest->{'op'}{$head} = Forest::Op->new( 'id' => $head,
						      'type' => 'adj',
						      'derivations' => $derivations,
						      'span' => $info->{'span'},
						      'cat' => $info->{'cat'},
						      'top' => &handle_avm($info->{'top'}),
						      'bot' => &handle_avm($info->{'bot'})
						  );
	} elsif ($info=$handler->{'std'}->($nt)) {
	    # Substitutions
	    &var_set($info->{'top'});
	    $forest->{'op'}{$head} = Forest::Op->new( 'id' => $head,
						      'type' => 'subst',
						      'derivations' => $derivations,
						      'span' => $info->{'span'},
						      'cat' => $info->{'cat'},
						      'top' => &handle_avm($info->{'top'})
						      );
	} elsif ($info=$handler->{'anchor'}->($nt)) {
	    # Anchors
	    &var_set($info->{'bot'});
	    $forest->{'op'}{$head} = Forest::Op->new( 'id' => $head,
						      'type' => 'anchor',
						      'derivations' => $derivations,
						      'span' => $info->{'span'},
						      'lex' =>$info->{'lex'} || "",
						      'bot' => &handle_avm($info->{'bot'}),
						      'cat' =>$info->{'cat'} || "",
						      ## 'tree' => $info->{'tree'},
						      lemma => $info->{lemma} || "",
						      lemmaid => $info->{lemmaid} || "",
						      token => $info->{token} || ""
						      );
	} elsif ($info=$handler->{'lexical'}->($nt)) {
	    # Lexicals
	    &var_set($info->{'bot'});
	    $forest->{'op'}{$head} = Forest::Op->new( 'id' => $head,
						      'type' => 'lexical',
						      'derivations' => $derivations,
						      'span' => $info->{'span'},
						      'lex' =>$info->{'lex'} || "",
						      'cat' =>$info->{'cat'} || "",
						      'bot' => &handle_avm($info->{'bot'}),
						      lemma => $info->{lemma} || "",
						      lemmaid => $info->{lemmaid} || "",
						      token => $info->{token} || ""
						    );
	  } elsif ($info=$handler->{'struct'}->($nt)) {
	    # Structs
	    my $op = $forest->{'op'}{$head} = Forest::Op->new( 'id' => $head,
							       'type' => 'struct',
							       'derivations' => $derivations,
							       'tree' => $info->{'tree'},
							     );
	    my $ht = $info->{ht};
	    if (defined $ht) {
	      &var_set($ht);
##	      $ht = &handle_avm($ht,'simplify');
	      $ht = &handle_avm($ht);
	      $ht->{type} = 'ht';
	      my $hid = "${sid}".$htid++;
	      $forest->{hypertag}{$hid} = Forest::Hypertag->new( id => $hid,
								 ht => $ht,
								 derivs => [],
								 tree => $op->{tree}
							       );
	      $op->{ht} = $hid;
	    }
	  } elsif ($nt =~ /^void_adj/) {
	    # deprecated old format
	  } elsif ($info=$handler->{'epsilon'}->($nt)){
	    $forest->{'op'}{$head} = Forest::Op->new( id => $head,
						      type => 'epsilon',
						      derivations => $derivations,
						      span => $info->{'span'},
						      lex =>$info->{'lex'} || "",
						      lemma => $info->{lemma} || "",
						      lemmaid => $info->{lemmaid} || "",
						      token => $info->{token} || ""
						    );
	} else {
##	    warn "Not a correct non terminal $nt";
	}
	($head > $forest->{'nb'}) and $forest->{'nb'}=$head;
    }
##    print "Read forest\n";
    ## A phase of normalization
    ## To assign to each derivation its associated tree
    foreach my $op ( grep {$_->{type} ne 'struct'} 
		     values %{$forest->{'op'}} ) {
      $_->decore($forest) foreach (@{$op->{derivations}});
    }
    %shared = ();
    %bodies = ();
    %comp = ();
    %reduce = ();
    %cache = ();
    my @delete = grep {$forest->{op}{$_} eq 'struct'} keys %{$forest->{op}};
    delete $forest->{op}{$_} foreach (@delete);
    ## to filter out some long derivations
    if (defined $keep) {
      foreach my $op (values %{$forest->{op}}) {
	my $derivs = $op->{derivations};
	if (scalar(@$derivs) > 1) {
	  my @tmp = grep {$_->keep($keep)} @$derivs;
	  if (@tmp && (scalar(@tmp) < scalar(@$derivs))) {
	    $op->{derivations} = [@tmp];
	    $op->{all_derivations} = $derivs;
	  }
	}
      }
    }
    return $forest;
}

sub Forest::Deriv::keep {
  my $self = shift;
  my $keep = shift;
  my $n = 0;
  foreach my $l (values %{$self->{nodes}}) {
    my $m = grep {defined $_ && $_->{type} eq 'adj'} map {$forest->{op}{$_}} @$l;
    ($m > $keep) and return 0;
    $n += $m;
  }
  return ($n < 20);
}

sub Forest::Deriv::decore {
  my ($self,$forest) = @_;
  $self->{decorated}++ and return $self;
  foreach my $u (values %{$self->{nodes}}) {
    foreach my $v (@$u) {
      my $k = (ref($v) eq 'Forest::Shared') ? $v->decore($forest) : $forest->{'op'}{$v};
      exists $k->{ht} and $self->{ht} = $k->{ht};
      exists $k->{tree} and $self->{tree} = $k->{tree}, return $self;
    }
  }
  return $self;
}

sub Forest::Shared::decore {
  my $self = shift;
  my $forest = shift;
  $self->{decorated}++ and return $self;
  foreach my $d (@{$self->{shared}}) {
    $d->decore($forest);
    exists $d->{ht} and $self->{ht} = $d->{ht};
    exists $d->{tree} and $self->{tree} = $d->{tree}, return $self;
  }
  return $self;
}

sub display_deriv {
  my $deriv = shift;
##  print STDERR "DERIV ";
  foreach my $key (sort keys %{$deriv->{nodes}}) {
    print STDERR "$key:{@{$deriv->{nodes}{$key}}} "
  }
##  print STDERR "\n";
}

sub sign_and_register {
  ## $body is an array (alternatives) of hash (derivs) of either
  ##      - label => [nt1,..,nt2]
  ##      - shared => [Forest::Shared({key => <key>, id => <id>, shared => <normbody>}),...]
  my $body = shift;
  my $sig = "";
  foreach my $deriv (@$body) {
    $sig .= "+$_=@{$deriv->{$_}}" foreach (sort keys %$deriv);
    $sig .= '|';
  }
  return $bodies{$sig} ||= body_normalize($body,$sig);
}

sub body_normalize {
  my ($body,$sig) = @_;
  my @all = ();
  ##  print "\t\tstart body normalize\n";
  my $max = 0;
  my $cutmax = $forest->{options}{keep};
  my $maxlist = $forest->{options}{maxlist} || 700;
#  my $cache = new Bloom::Filter(capacity => 100000, error_rate => 0.0001);
  my %cache = ();
  foreach my $deriv (@$body) {
    ## **WARNING** : the following may reduce the size of the forest
    ## and the best parse may be lost !
    ## should use a parameter
    my $allderivs = [{}];
    foreach my $label (keys %$deriv) {
      my $nodes = $deriv->{$label};
      if (!ref($nodes->[0])) {
	my $v = $comp{simple}{"@$nodes"} ||= $nodes; 
	foreach my $d1 (@$allderivs) {
	  my $x1 = $d1->{$label};
	  $d1->{$label} = $x1 ? ($comp{"$x1$v"} ||= [@$x1,@$v]) : $v;
	}
      } else {
	foreach my $node (@$nodes) {
	  my $firstd2;
	  my @tmp = ();
	  foreach my $d2 (map {$_->{nodes}} @{$node->{shared}}) {
	    $firstd2 or $firstd2 = $d2, next;
	  D1:
	    foreach my $d1 (map {{%$_}} @$allderivs) {
	      foreach (keys %$d2) {
		my $x = $d1->{$_} = $d1->{$_} 
		  ? ($comp{"$d1->{$_}@{$d2->{$_}}"} ||= [@{$d1->{$_}},@{$d2->{$_}}])  
		    : $d2->{$_};
		@$x < $cutmax or next D1;
	      }
	      push(@tmp,$d1);
	    }
	  }
	  foreach my $d1 (@$allderivs) {
	    foreach (keys %$firstd2) {
	      $d1->{$_} = $d1->{$_} 
		?  ($comp{"$d1->{$_}@{$firstd2->{$_}}"} ||= [@{$d1->{$_}},@{$firstd2->{$_}}])  
		  : $firstd2->{$_};
	    }
	  }
	  if (@tmp) {
	    unshift(@tmp,@$allderivs);
	    $allderivs = \@tmp;
	  }
	}
      }
    }
  DERIV:
    foreach my $deriv (@$allderivs) {
      ## We keep a bounded number of derivations
      ## To select those to be kept, we use the $cutmax dynamic parameter
      ## The derivations with more than $cutmax operations (mostly adjoining) on a node
      ## are discarded.
      ## $cutmax is progressively decreased when necessary to keep a short deriv list
      my $dmax = 0;
#      my $dsum = 0;
      my $dsig = "";
      foreach (values %$deriv) {
	@$_ < $cutmax or next DERIV;
	@$_ > $dmax and $dmax = @$_;
#	$dsum += @$_;
	$dsig .= "@$_";
      }
#      $cache->check($dsig) and next DERIV;
      $cache{$dsig} and next DERIV;
      # inlined construction of Forest::Deriv objects !
      my $d = bless { nodes => $deriv, 
		      max => $dmax, 
#		      sum => $dsum,
#		      avg => $dsum / scalar(keys %$deriv)
		    }, 'Forest::Deriv';
      if ($cutmax > 1 && scalar(@all) == $maxlist) {
	## print STDERR "potential cut at max=$max dmax=$dmax cutmax=$cutmax\n";
	$cutmax = $max;
	($dmax < $max) or next;
	@all = grep { $_->{max} < $max } @all;
	$max--;
	## print STDERR "cutting max=$max\n";
	if (0 && $max < 2) {	# debug
	  print STDERR "sig is $sig\n";
	  foreach my $d (@all) {
	    my $deriv = $d->{nodes};
	    print STDERR "deriv\n";
	    foreach (keys %$deriv) {
	      print STDERR "\t$_ : @{$deriv->{$_}}\n";
	    }
	  }
	}
      } else {
	$dmax > $max and $max = $dmax;
      }
      $cache{$dsig} = 1;
#      $cache->add($dsig);
      push(@all,$d);
    }
  }
  ##  print "\t\tdone body normalize\n";
  return \@all;
}

sub new_deriv_with_max {
  my $nodes = shift;
  my $cutmax = shift;
  my $max = 0;
  my $sum = 0;
  foreach (values %$nodes) {
    @$_ < $cutmax or return undef;
    @$_ > $max and $max = @$_;
    $sum += @$_;
  }
#  if ($max > 8 || $sum > 15) {
#    print STDERR "big max=$max sum=$sum\n";
#  }
  return bless { nodes => $nodes, max => $max }, 'Forest::Deriv';
}

sub handle_avm {
  my $avm = shift || '';
  my $simplify = shift || 0;
  return $cache{$simplify}{$avm} ||= fsargs_parse($avm,$simplify);
}

sub handle_value {
    shift;
    s/^\s*//o;
    s/\s*$//;
    return fs_value($_);
}

sub var_set {
  my $p = shift;
  $p or return;
  $vars{$_}++ foreach ($p =~ /[A-Z_]\w*/og);
}

package Forest::LP::XRCG::Parser;

sub parse {
    $Forest::LP::Parser::type='xrcg';
  my $forest = Forest::LP::Parser::parse(@_);
    $Forest::LP::Parser::type='lp';
    return $forest;
}

1;
__END__

=head1 NAME

Forest::LP::Parser - Parse the LP representation of TAG Derivation forest

=head1 SYNOPSIS

  use Forest::LP::Parser;

=head1 DESCRIPTION

=head2 EXPORT

None by default.

=head1 SEE ALSO

Forest (3)
perl(1).

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2008, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut


sub handle_avm {
    my %fs = ();
    my $avm = shift || '';
    $avm =~ /^/g;
    while ($avm =~ /\G\s*(\w+)\s*=>/gc) {
	my $f = $1;
	my $var = "";
	my $val = "";
	my @trail = ();
	$var = $1 if ($avm =~ /\G\s*<([A-Z]\w*)>\s*(::)?/gc);
	$val = $1 if ($avm =~ /\G\s*([[:alnum:]+-]+)/gc);
	@trail = split(',',$1) if ($avm =~ /\G[\{\[](.*?)[\}\]]/gc);
	$avm =~ /\G\s*,/gc;
	$var="" if ($var && $vars{$var} == 1);
	@trail = ($val) if (!@trail && $val);
	@trail = map( &handle_value($_), @trail);
	my $comp = Forest::Comp->new( 'comp' => $f, 
				      'value' => [@trail] );
	$var and $comp->{id} = $var;
	$fs{$f}=$comp;
    }
    return Forest::Feature->new( 'f' => \%fs );
}
