# $Id#
package Forest::HTML::Writer;

require 5.005_62;
use strict;
use warnings;
use Forest;
use CGI qw/:standard *pre *table :html3/;

our $vars;
our $forest;

sub var_clean {
    undef $vars;
    $vars = { 'counter' => 0,
	      'names' => {}
	  };
}

sub var_name {
    my $var = shift;
    my $name = "";
    if (defined $vars->{names}{$var}) {
	$name = $vars->{names}{$var};
    } else {
	$name = $vars->{names}{$var} = $vars->{counter}++;
    }
    return "\@$name";
}

sub Forest::html {
    my $self = shift;
    my @sentence = ();
    my $nb = $self->{'nb'};
    $forest = $self;
    my $i=0;
    my $sentence=$self->{'sentence'} || "";
    $sentence =~ s/^\s*//o;
    $sentence =~ s/\s*$//o;
    foreach my $word (split(/\s+/,$sentence)) {
      $word=(split(/:/,$word))[0];
      push(@sentence,Sub(font({color=>'red'},$i++)),$word);
    }
    push(@sentence,Sub(font({color=>'red'},$i)));
    print CGI::div({align=>'center'},@sentence),hr();
    print start_table({cellpadding=>0,width=>'17cm'});
    foreach my $head (0..$nb) {
	$self->{'op'}{$head}->html if (defined $self->{'op'}{$head});
    }
    print end_table,"\n";
}

sub treename {
  my $treename = shift;
  return (split(/\s+/,$treename))[0];
}

sub Forest::Op::html {
    my $self = shift;
    my $type = $self->{'type'};
    my $head = $self->{'id'};
    my @entry;

    &var_clean;
    if ($type eq 'anchor') {
      my $label = $self->{'lex'};
      $label .= "/$self->{lemma}" if (defined $self->{lemma});
      $label .= ":$self->{'cat'}" if (defined $self->{cat});
      my $bot = (defined $self->{bot}) ? ($self->{bot}->html) : "";
	@entry=(&display_span(font({color=>'magenta'},$label),@{$self->{'span'}}),
		$bot,
		font({color=>'blue'},treename($self->{'tree'})),
		"");
    } elsif ($type eq 'lexical' ) {
      my $label = $self->{'lex'};
      $label .= "/$self->{lemma}" if (defined $self->{lemma});
      $label .= ":$self->{'cat'}" if (defined $self->{cat});
      my $bot = (defined $self->{'bot'}) ? ($self->{'bot'}->html) : "";
      @entry= (&display_span(font({color=>'magenta'},$label),@{$self->{'span'}}),
	       $bot,
	       "",
	       "");
    } elsif ($type eq 'subst' ) {
	@entry= (&display_span($self->{'cat'},@{$self->{'span'}}),
		 $self->{'top'}->html,
		 "",
		 "");
    } elsif ($type eq 'adj' ) {
	my ($tl,$tr,$bl,$br) = @{$self->{'span'}};
	@entry= ( &display_span($self->{'cat'},$tl,$tr), 
		  $self->{'top'}->html,
		  &display_span($self->{'cat'},$bl,$br), 
		  $self->{'bot'}->html
		  );
    } else {
	die "not a valid op type $type";
    }
    my $derivation = table(Tr(td([$head,
				  ' <-- ', 
				  table(Tr([map( $_->html, @{$self->{'derivations'}})
					    ]))])));
    push(@entry,$derivation);
    print Tr({ valign=>'middle' },
	     td({align=>'left',nowrap=>''},\@entry));
}

sub display_span {
    my ($cat,$l,$r) = @_;
    return "$cat".Sub(font({color=>'red'},"$l-$r"));
}

sub Forest::Deriv::html {
    my $self = shift;
    my %nodes = %{$self->{'nodes'}};
    my @content = ();
    my @edges = ();

    foreach my $label (keys %nodes){
	foreach my $op (@{$nodes{$label}}) {
	    next unless (defined $forest->{'op'}{$op});
	    my $anchor = $forest->{'op'}{$op}{'type'} eq 'anchor';
	    my $color = $anchor ? 'magenta' : 'green';
	    if ($label =~ /^\d+/) {
		my $cat =  $forest->{'op'}{$op}{'cat'};
		$label = "$cat\#$label";
	    }
	    push(@edges, 
		 { 'span' => $forest->{'op'}{$op}{'span'},
		   'type' => $forest->{'op'}{$op}{'type'},
		   'color' => $color,
		   'label' => $label,
		   'node' => $op}
		 );
	}
    }

    foreach my $edge (sort edge_sort @edges) {
	push(@content,
	     font({color=> $edge->{'color'}},"[$edge->{'label'}]"),
	     $edge->{'node'});
    }

    return td( join(' ',@content) );
}

sub edge_sort {
    my @spana = @{$Forest::HTML::Writer::a->{'span'}};
    my @spanb = @{$Forest::HTML::Writer::b->{'span'}};
    my $typea = $Forest::HTML::Writer::a->{'type'};
    my $typeb = $Forest::HTML::Writer::b->{'type'};
    if ($typea ne $typeb) {
	return ($typea eq 'subst') ? -1 : 1;
    } elsif ($spana[0] != $spanb[0]) {
	return $spana[0] cmp $spanb[0];
    } else {
	return $spana[1] cmp $spanb[1];
    }
}


sub Forest::Feature::html {
    my $self = shift;
    my $features = $self->{'f'};
    my @content;
    foreach my $f (sort keys %$features) {
	push @content, $features->{$f}->html;
    }
    my $table = table({align=>'vcenter',border=>2,cellpadding=>0},
		      Tr([ @content ])
		     );
    $table = table(td([font({color=>'orange'},$self->{type}),$table])) if (defined $self->{type});
    return $table;
  }

sub Forest::Comp::html {
    my $self = shift;
    my @content = ();
    my $value = join(' | ',
		     map($_->html,@{$self->{'value'}}));
    if (defined $self->{'id'}) {
	push @content, &var_name($self->{'id'});
	push @content, '::' if ($value);
    }
    $value = join('',@content,$value);
    return unless ($value);
    return 
      span(td(font({color=>'green'},$self->{'comp'})), 
	   td({-align=>'center'},font({color=>'blue'},$value)));
}

sub Forest::Val::html {
    $_ =  shift->{'val'};
    s/^plus/+/;
    s/^moins/-/;
    return $_;
}

sub Forest::Plus::html {
    return '+';
}

sub Forest::Minus::html {
    return '-';
}


1;
__END__

=head1 NAME

Forest::HTML::Writer

=head1 DESCRIPTION

=head1 SYNOPSIS

  use Forest::HTML::Writer;

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

