# $Id$
package Forest::Line::Writer;

require 5.005_62;
use strict;
use warnings;
use Forest;

our $forest;

sub Forest::line {
    my $self=shift;
    $forest = $self;
    foreach my $head (keys %{$self->{'op'}}) {
	$self->{'op'}{$head}->line();
    }
}

sub Forest::Op::line {
    my $self=shift;
    return if ($self->{'type'} eq 'anchor');
    my $info = "{$self->{'type'} $self->{'cat'} @{$self->{'span'}}}";
    foreach my $deriv (@{$self->{'derivations'}}) {
	$deriv->line($info);
    }
}

sub Forest::Deriv::line {
    my $self=shift;
    my $opinfo = shift;
    my @anchor;
    my $tree;
    foreach my $node (keys %{$self->{'nodes'}}) {
	foreach my $id2 (@{$self->{'nodes'}{$node}}) {
	    my $op2 = $forest->{'op'}{$id2};
	    next unless ($op2->{'type'} eq 'anchor');
	    $tree = $op2->{'tree'} unless (defined $tree);
	    push(@anchor,"$node={$op2->{'lex'} $op2->{'cat'} @{$op2->{'span'}}}");
	}
    }
    @anchor = sort @anchor;
    print join(' ',$opinfo,"($tree @anchor)"),"\n";
    foreach my $node (sort keys %{$self->{'nodes'}}) {
	foreach my $id2 (@{$self->{'nodes'}{$node}}) {
	    my $op2 = $forest->{'op'}{$id2};
	    next if ($op2->{'type'} eq 'anchor');
	    $op2 = "{$op2->{'type'} $op2->{'cat'} @{$op2->{'span'}}}";
	    print join(' ',$opinfo,"($tree @anchor)","$node=$op2"),"\n";
	}
    }
}

1;

__END__

=head1 NAME

Forest::Line::Writer

=head1 DESCRIPTION

=head1 SYNOPSIS

  use Forest::Line::Writer;

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

