# $Id$
package Forest::Line::Parser;

require 5.005_62;
use strict;
use warnings;
use Forest;

our $forest;
our %op;
our %deriv;

sub parse {
    local *IN = shift;
    $forest = Forest->new( {@_, 'op'=>{} } );
    $forest->{'nb'} = 0;
    %op = ();
    %deriv = ();
    while (<IN>) {
	next if /^\#/;
	next if /^\s*$/;
	my ($op,$anchor,$comp) =
	    m{^\{(.*?)\}\s+	# op info
		  \((.*?)\)\s+	# anchor info
		      (.*)\s*	# comp info
		      }xo;
	if ($comp) {
	    add_comp(add_anchor(add_op($op),$anchor),$comp);
	} else {
	    add_anchor(add_op($op),$anchor);
	}
    }
    return $forest;
}

sub add_op {
    my $key = shift;
    if (defined $op{$key}) {
	return $op{$key};
    }
    my $nb = $forest->{'nb'}++;
    my ($type,$cat,@span) = split(/\s+/,$key);
    my $op = Forest::Op->new( 'id' => $nb,
			      'type' => $type,
			      'cat' => $cat,
			      'span' => [@span],
			      'derivations' => [] );
    $forest->{'op'}{$nb} = $op;
    $op{$key} = $nb;
    return $nb;
}

sub add_anchorop {
    my $treea = shift;
    my $key = shift;
    $key = "anchor $treea $key";
    if (defined $op{$key}) {
	return $op{$key};
    }
    my $nb = $forest->{'nb'}++;
    my ($type,$tree,$lex,$cat,@span) = split(/\s+/,$key);
    my $op = Forest::Op->new( 'id' => $nb,
			      'type' => $type,
			      'tree' => $tree,
			      'lex' => $lex,
			      'cat' => $cat,
			      'span' => [@span],
			      'derivations' => [] );
    $forest->{'op'}{$nb} = $op;
    $op{$key} = $nb;
    return $nb;
}

sub add_anchor {
    my $opref = shift;
    my $key = shift;
    if (defined $deriv{$opref}{$key}) {
	return $deriv{$opref}{$key};
    }
    my $op = $forest->{'op'}{$opref};
    my ($tree,$anchors) = ($key =~ /^(\S+)\s+(.*)/);
    my $deriv = Forest::Deriv->new( 'nodes' => {} );
    push(@{$op->{'derivations'}},$deriv);
    $deriv{$opref}{$key} = $deriv;
    foreach my $anchor  ($anchors =~ /(\S+=\{.*?\})/og) {
	my ($name,$anchorop) = ($anchor =~ /^(\S+)?=\{(.*?)\}/);
	push(@{$deriv->{'nodes'}{$name}},
	     add_anchorop($tree,$anchorop));
    }
    return $deriv;
}

sub add_comp {
    my $deriv = shift;
    my $comp = shift;
    my ($name,$opref) = ($comp =~ /^(\S+)=\{(.*)\}/);
    push(@{$deriv->{'nodes'}{$name}},add_op($opref));
}

1;

__END__

=head1 NAME

Forest::Line::Parser

=head1 SYNOPSIS

  use Forest::Line::Parser;

=head1 DESCRIPTION

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

