# $Id$
package Forest::Tagger::Writer;

require 5.005_62;
use strict;
use warnings;
use Forest;

our $forest;

our $words;

sub Forest::tagger {
  my $self=shift;
  my $nb = $self->{'nb'};
  
  # First pass: collecting tags
  $self->collect_tags;
  
  # Second pass: emiting
  foreach my $left (sort {$a <=> $b} keys %$words) {
    foreach my $right (sort {$a <=> $b} %{$words->{$left}}) {
      my $entry = $words->{$left}{$right};
      foreach my $lex (sort keys %$entry) {
	foreach my $cat (sort keys %{$entry->{$lex}}) {
	  foreach my $fs (sort keys %{$entry->{$lex}{$cat}}) {
	    print "$left\t$right\t$cat\t$lex\t$fs\n";
	  }
	}
      }
    }
  }

}

sub Forest::collect_tags {
  my $self = shift;
  $words = {};
  foreach my $head (keys %{$self->{'op'}}) {
    $self->{'op'}{$head}->collect_tags($head);
  }
}

sub Forest::Op::collect_tags {
  my $self = shift;
  my $head = shift;
  my $type = $self->{type};
  return unless ($type eq 'lexical' || $type eq 'anchor');
  my $cat = $self->{cat};
  my $span = $self->{span};
  my $fs = '';
  $fs = $self->{bot}->flat() if (defined $self->{bot});
##  print "BOT $fs\n";
  $words->{$span->[0]}{$span->[1]}{$self->{lex}}{$cat}{$fs} ||= 1;
}

sub Forest::Feature::flat {
  my $self = shift;
  my $features = $self->{'f'};
  my @content = ();
  foreach my $f (sort keys %$features) {
    push(@content,$features->{$f}->flat);
  }
  return '['.join(', ',@content).']';
}

sub Forest::Comp::flat {
    my $self = shift;
    my @content = ();
    my $value = join('|',map($_->flat,@{$self->{value}}));
    return unless $value;
    return "$self->{comp} : $value";
}

sub Forest::Val::flat {
  shift->{val};
}

sub Forest::Plus::flat {
    return '+';
}

sub Forest::Minus::flat {
    return '-';
}

1;

__END__

=head1 NAME

Forest::Tagger::Writer

=head1 DESCRIPTION

=head1 SYNOPSIS

  use Forest::Tagger::Writer;

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut
