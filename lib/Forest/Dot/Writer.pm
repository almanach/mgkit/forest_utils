#$Id$
package Forest::Dot::Writer;

require 5.005_62;
use strict;
use warnings;
use Forest;

our $forest;
our %done;

our %color = ( 'subst'   => 'blue',
	       'adj'     => 'red',
	       'anchor'  => 'yellow',
	       'lexical' => 'magenta',
	       'epsilon' => 'green'
	     );

sub treename {
  my $treename = shift;
  return (split(/\s+/,$treename))[0];
}

sub Forest::dot {
    my $self     = shift;
    my $nb       = $self->{'nb'};
    my @content  =();


    %done = ();
    $forest = $self;

    print <<HEADER;
digraph G {
        center = 1;
	ranksep = 1.5;
	ordering=out;
	node [shape=record, fontsize=8.0];
HEADER

    foreach my $head (0..$nb){
	next unless (defined $self->{'op'}{$head} && !defined $done{$head});
	$self->{'op'}{$head}->dot();
    }
	
	print "}\n\n";

}

sub Forest::Op::dot {
    my $self=shift;
    my $depth=shift;
    my $head = $self->{'id'};
    return if ($self->{type} eq 'anchor');
    return if ($done{$head});
    $done{$head}='yes';
    if ($self->{type} eq 'lexical'){
	my $span = join('_',@{$self->{'span'}});
	my $lex = $self->{lex};
	$lex =~ s/"/\\"/og;
    print <<NODE;
\top$head [ label = "{<root> $lex:$self->{cat}_$span}" ];
NODE
      } elsif ($self->{type} eq 'epsilon'){
	my $span = join('_',@{$self->{'span'}});
	my $lex = $self->{lex};
	$lex =~ s/"/\\"/og;
    print <<NODE;
\top$head [ label = "{<root> $lex:$span}" ];
NODE
    } else {
	my $i = 0;
	my $deriv = join('|',map($_->dot($head,$i++), @{ $self->{'derivations'}}));
	my $span = join('_',@{$self->{'span'}});
    print <<NODE;
\top$head [ label = "{ <root> $self->{cat}_$span| { $deriv }}" ];
NODE
}
}

sub Forest::Deriv::dot {
    my $self=shift;
    my $op=shift;
    my $index=shift;
    my @anchor;
    my @nodes =();
    my $id="op$op:der$index";
    my @edges=();
    foreach my $label (keys %{$self->{'nodes'}}){
	foreach my $node (@{$self->{'nodes'}{$label}}){
	    next unless (defined $forest->{'op'}{$node});
	    my $target = $forest->{'op'}{$node};
	    if ($target->{'type'} eq 'anchor'){
		push(@anchor,$target);
		next ;
	    }
	    my $color =  $color{$target->{'type'}};
	    if ($label =~ /^\d+/) {
		my $cat =  $target->{'cat'};
		$label = "$cat\#$label";
	    }
	    push(@edges, { 'span' => $target->{'span'},
			   'type' => $target->{'type'},
			   'color' => $color,
			   'label' => $label,
			   'node' => $node
			   } 
		 );
	}
    }
    foreach my $edge (sort edge_sort @edges) {
	&edge_print($edge,$id);
    }
    my $tree = treename($self->{'tree'});
    my $lex = join(',',map("$_->{'lex'}:$_->{'cat'}",@anchor));
    return "<der$index> $tree($lex)";
}

sub edge_print {
    my $edge = shift;
    my $id = shift;
    print <<EDGE;
\t$id -> op$edge->{'node'}:root [label = "$edge->{'label'}", color=$edge->{'color'}];
EDGE
}

sub edge_sort {
    my @spana = @{$Forest::Dot::Writer::a->{'span'}};
    my @spanb = @{$Forest::Dot::Writer::b->{'span'}};
    my $typea = $Forest::Dot::Writer::a->{'type'};
    my $typeb = $Forest::Dot::Writer::b->{'type'};
    if ($typea ne $typeb) {
	return ($typea eq 'subst') ? -1 : 1;
    } elsif ($spana[0] != $spanb[0]) {
	return $spana[0] cmp $spanb[0];
    } else {
	return $spana[1] cmp $spanb[1];
    }
}


1;
__END__

=head1 NAME

Forest::Dot::Writer

=head1 DESCRIPTION

=head1 SYNOPSIS

  use Forest::Dot::Writer;

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

