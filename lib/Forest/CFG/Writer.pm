package Forest::CFG::Writer;

## to be used to generate a Context-Free Grammar that approximates the TAG derivation for a sentence
## By consulting the set of CFG produced by FRMG over a corpus, it should be possible to derive a PCFG

require 5.005_62;
use strict;
use warnings;
use Forest;

our $forest;
our $closedPOS = {
		  'cln' => 1,
		  'cla' => 1,
		  'cld' => 1,
		  'clr' => 1,
		  'cll' => 1,
		  'clg' => 1,
		  'clneg' => 1,
		  'adv' => 1,
		  'advneg' => 1,
		  'ilimp' => 1,
		  'advneg' => 1,
		  'ponctw' => 1,
		  'poncts' => 1,
		  'pro' => 1,
		  'xpro' => 1,
		  'adv' => 1,
		  'que' => 1,
		  'pri' => 1,
		  'prel' => 1,
		  'det' => 1,
		  'predet' => 1,
		  'caimp' => 1,
		  'ce' => 1,
		  'que_restr' => 1,
		  'ce' => 1
		 };

sub Forest::CFG {
  my $self = shift;
  foreach my $r ($self->CFGaux) {
    print "$r->[0] <-- ".join(' ',@{$r->[1]})."\t%% $r->[2] <--".join(" ",@{$r->[3]})."\ttree=$r->[4]\n"
  }
}

sub Forest::CFGaux {
  my $self = shift;
  $forest = $self;
  my $nb = $self->{'nb'};
  my $i=0;
  my $sentence=$self->{'sentence'} || "";
  $sentence =~ s/^\s*//o;
  $sentence =~ s/\s*$//o;
  my @rules = ();
  foreach my $head (0..$nb) {
    (defined $self->{'op'}{$head}) or next;
    push(@rules,$self->{'op'}{$head}->CFG);
  }
  foreach my $head (0..$nb) {
    (defined $self->{'op'}{$head}) or next;
    my $op = $self->{op}{$head};
    exists $op->{cfg} and next;
    push(@rules,$op->CFGroot);
  }
  return @rules;
}

sub Forest::Op::CFG {
  my $self = shift;
  my $type = $self->{'type'};
  my $id = $self->{'id'};
  my @span = @{$self->{span}};
  my @modes = ();
  if ($type eq 'subst'){
    push(@modes,'');
  } elsif ($type eq 'adj') {
    push(@modes,qw{left right});
  }
  ## other Op kinds (anchor and lexical) provide terminals
  my @rules = ();
  foreach my $mode (@modes) {
    if (my $x = $self->CFGnt($mode)) {
      my ($min,$max,$head) = @$x;
      my @bodies = map {$_->CFG($min,$max)} @{$self->{derivations}};
      foreach my $body (@bodies) {
	##	print "$head <-- $body->[0].\t%% $min:$max <-- $body->[1]\n";
	my $xhead = "$body->[2]_$head";
	push(@rules,[$xhead,$body->[0],"$min:$max",$body->[1],$body->[2]]);
      }
    }
  }
  return @rules;
}

sub Forest::Op::CFGroot {
  my $self = shift;
  my $type = $self->{'type'};
  ($type eq 'subst') or return;
  my $x = $self->CFGnt('');
  my ($min,$max,$head) = @$x;
  my @rules = ();
  foreach my $deriv (@{$self->{derivations}}) {
    my ($tree) = $deriv->{tree}  =~ /^(\S+)/;
    my $xhead = "${tree}_$head";
    push(@rules,[$head,[$xhead],"$min:max",[],$tree]);
  }
  return @rules;
}

sub Forest::Deriv::CFG {
  my $self = shift;
  my ($min,$max) = @_;
  my $nodes = $self->{nodes};
  my ($tree) = $self->{tree} =~ /^(\S+)/;
  my @body = ();
  foreach my $label (keys %$nodes) {
    foreach my $oid (@{$nodes->{$label}}) {
      (exists $forest->{op}{$oid}) or next;
      my $op = $forest->{op}{$oid};
      $op->{cfg} = 1;
      my $type = $op->{type};
      my $derivs = $op->{derivations};
      my @modes = ();
      if ($type eq 'adj') {
	push(@modes,qw{left right});
      } else {
	push(@modes,'');
      }
      foreach my $mode (@modes) {
	my $nt = $op->CFGnt($mode);
	(defined $nt) or next;
	($nt->[0] < $min || $nt->[1] > $max) and next;
	if (@{$derivs}) {
	  ## warning: adding tree info on body components is only ok for derivation trees, not for derivation forests
	  ## the algo would be more complex for forest to get flat bodies
	  my $tderiv = $derivs->[0];
	  my ($ttree) = $tderiv->{tree} =~ /^(\S+)/;
	  $nt->[2] = "${ttree}_$nt->[2]";
	}
	push(@body,$nt);
      }
    }
  }
  @body = sort {$a->[0] <=> $b->[0] || $a->[1] <=> $b->[1]} @body;
  my @body1 = map {$_->[2]} @body;
  @body1 or push(@body1,'true');
  my @body2 = map {"$_->[0]:$_->[1]"} @body;
  return [\@body1,\@body2,$tree];
}

sub Forest::Op::CFGnt {
  my $self = shift;
  my $type = shift;
  my $optype = $self->{'type'};
  my $label = $self->{cat};
  if ($optype eq 'anchor') {
    $label = "\$$label";
  } elsif (!$label || $label eq '_' || ($optype eq 'lexical' && exists $closedPOS->{$label})
	  ) {
    if ($self->{token}) {
      $label = "token<$self->{token}>";
    } else {
      $label = "void";
    }
  } elsif ($optype eq 'lexical') {
    $label = "\$$label";
  }
  $type and $label .= "_$type";
  my @nt = ();
  my @span = @{$self->{span}};
  if (@span == 2) {
    @nt=@span;
    # nothing to do
  } elsif ($span[0] == $span[1] && $span[2] == $span[3] && $span[1] == $span[2]) {
    # empty adj
    ($type eq 'right') or return undef;
    @nt=@span[0,1];
  } elsif ($span[0] == $span[1]) {
    # left adj
    ($type eq 'left') or return undef;
    @nt=@span[2,3];
  } elsif ($span[2] == $span[3]) {
    # right adj
    ($type eq 'right') or return undef;
    @nt=@span[0,1];
  } else {
    # full adj
    if ($type eq 'left') {
      @nt=@span[0,2];
    } else {
      @nt=@span[3,1];
    }
  }
##  print "%% $label between @nt\n";
  return [@nt,$label];
}

1;

__END__

Forest::CFG::Writer

=head1 DESCRIPTION

=head1 SYNOPSIS

  use Forest::CFG::Writer;

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2015, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut
