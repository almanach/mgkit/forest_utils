# $Id$
package Forest::Info::Writer;

require 5.005_62;
use strict;
use warnings;
use Forest;

our $forest;
our %info = ();

sub Forest::info {
  my $self = shift;
  $forest = $self;
  my $nb = $self->{'nb'};
  foreach my $head (0..$nb){
    next unless (defined $self->{'op'}{$head});
    my $nbderiv = $self->{op}{$head}->info;
    print "$head: $nbderiv derivations\n";
  }
}

sub Forest::Op::info {
  my $self = shift;
  my $head = $self->{id};
  my $nbderiv = 0;
  unless (exists $info{$head}) {
    foreach my $deriv (@{ $self->{derivations} }) {
      $nbderiv = $nbderiv + $deriv->info;
    }
    $nbderiv = 1 unless ($nbderiv); #  at least one derivation
    $info{$head} = $nbderiv;
  }
  return $info{$head};
}

sub Forest::Deriv::info {
  my $self = shift;
  my $nbderiv = 1;
  foreach my $label (keys %{$self->{nodes}}){
    foreach my $node (@{$self->{nodes}{$label}}) {
      next unless (defined $forest->{op}{$node});
      my $op = $forest->{'op'}{$node};
      $nbderiv = $nbderiv * $op->info;
    }
  }
  return $nbderiv;
}

1;

__END__

=head1 NAME

Forest::Info::Writer - Emit information about a shared derivation forest

=head1 SYNOPSIS

  use Forest::Info::Writer;

=head1 DESCRIPTION

=head2 EXPORT

None by default.

=head1 SEE ALSO

perl(1).

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

