# $Id$
package Forest::XML::Writer;

require 5.005_62;
use strict;
use warnings;
use Forest;
use XML::Generator;

our $GEN;
our $level;
our %done=();
our $forest;

sub indent {
    my ($level,$list) = @_;
    return unless @$list;
    return (map( ("\n","  " x ($level+1),$_), @$list),"\n","  " x $level );
}

sub Forest::pretty_print {
    my $self=shift;
    my $nb = $self->{'nb'};
    my @content=();

    $level=shift || 1;
    %done=();
    $forest=$self;
    $GEN= new XML::Generator('encoding' => 'ISO-8859-1', 
			     ':pretty' => 1);

    push(@content,$GEN->sentence($self->{'sentence'})) if (defined $self->{'sentence'});

    foreach my $head (0..$nb){
	next unless (defined $self->{'op'}{$head} && !defined $done{$head});
	push(@content,
	     $self->{'op'}{$head}->pretty_print(0),
	     );
    }

    print         
      $GEN->xmldecl(), 
	$GEN->xmldtd([qw{forest PUBLIC
			 "-//INRIA//DTD TAG Derivation Forest 1.0//EN"},
		      '"http://alpage.inria.fr/~clerger/forest.dtd,xml"'])
      ;
    
    my %attr = ();
    $attr{'parser'} = $self->{'parser'} if (defined $self->{'parser'});
    $attr{'time'} = $self->{'time'} if (defined $self->{'time'});
    $attr{'mode'} = $self->{'mode'} if (defined $self->{'mode'});

    print $GEN->forest(\%attr,@content)
	,"\n";
}

sub Forest::Op::pretty_print {
    my $self=shift;
    my $depth=shift;
    my @content=();
    my $head = $self->{'id'};
    return if ($self->{type} eq 'anchor' || $self->{type} eq 'struct');
    return $GEN->opref({'ref'=>$head}) if ($done{$head} || $depth > 3*$level);
    $done{$head}='yes';
    push(@content,$self->{'top'}->pretty_print($depth+1,'top')) if (defined $self->{'top'});
    push(@content,$self->{'bot'}->pretty_print($depth+1,'bot')) if (defined $self->{'bot'});
    push(@content,map( $_->pretty_print($depth+1),@{ $self->{'derivations'} }));
    return $GEN->op({ type=> $self->{type},
		      cat=> $self->{cat},
		      lex=> $self->{lex},
		      lemma => $self->{lemma},
		      span=> join(' ',@{$self->{'span'}}),
		      id=>$head
		      },
		    @content
		    );
}

sub Forest::Feature::pretty_print {
    my $self=shift;
    my $depth=shift || 2;
    my $type=shift || undef ;
    my $features = $self->{'f'};
    my @content;
    foreach my $f (sort keys %$features) {
	push(@content, $features->{$f}->pretty_print);
    }
    if (defined $type) {
      return unless (@content);
      @content = ($GEN->fs( @content ));
      if ($type eq 'ht') {
	return $GEN->hypertag(@content);
      } else {
	return $GEN->narg({'type'=>$type},@content );
      }
    } else {
      return $GEN->fs({type=>$self->{type}},@content);
    }
}

sub Forest::Comp::pretty_print {
    my $self=shift;
    my %attr = ( 'name' => $self->{'comp'});
    return unless (@{$self->{'value'}} || defined $self->{'id'});
    $attr{'id'} = $self->{'id'} if (defined $self->{'id'});
    return $GEN->f( \%attr, map( $_->pretty_print, @{ $self->{'value'} } ));
}

sub Forest::Plus::pretty_print {
    return $GEN->plus;
}

sub Forest::Minus::pretty_print {
    return $GEN->minus;
}

sub xml_escape {
  my $s = shift;
  $s =~ s/\&/&amp;/og;
  $s =~ s/</&lt;/og;
  $s =~ s/>/&gt;/og;
  $s =~ s/"/&quot;/og;
  return $s;
}

sub Forest::Val::pretty_print {
    return $GEN->val(xml_escape(shift->{'val'}));
}

sub Forest::Deriv::pretty_print {
  my $self=shift;
  my $depth=shift;
  my @nodes =();
  foreach my $label (keys %{$self->{'nodes'}}){
    foreach my $node (@{$self->{'nodes'}{$label}}) {
      next unless (defined $forest->{'op'}{$node});
      my $op = $forest->{'op'}{$node};
      if ($op->{'type'} eq 'anchor'){
	my %attr = ('type'=>'anchor', id =>$op->{id});
	$attr{lex} = $op->{lex} if (defined $op->{lex});
	$attr{lemma} = $op->{lemma} if (defined $op->{lemma});
	$attr{cat} = $op->{cat} if (defined $op->{cat});
	$attr{span} = join(' ',@{$op->{span}}) if (defined $op->{span});
	my @content = ();
	push(@content,$op->{bot}->pretty_print($depth+1,'bot')) if (defined $op->{bot});
	push(@nodes,$GEN->node({'name'=>$label},
			       $GEN->op({%attr},@content)));
	next ;
      }
      push(@nodes,
	   $GEN->node({'name'=>$label},
		      $op->pretty_print($depth+1)));
    }
  }
  foreach my $shared (@{$self->{'nodes'}{'shared'}}){
    push(@nodes,$shared->pretty_print($depth));
  }
  my %args = ();
  $args{tree} = $self->{tree} if (defined $self->{tree});
  if (defined $self->{ht} && exists $forest->{hypertag}{$self->{ht}}) {
    unshift(@nodes, 
	    $forest->{hypertag}{$self->{ht}}->pretty_print($depth+1,'ht'));
  }
  return $GEN->deriv({ %args  }, @nodes );
}

sub Forest::Shared::pretty_print {
    my $self=shift;
    my $depth=shift;
    return $GEN->shared({id=>$self->{key}},map($_->pretty_print($depth),@{$self->{shared}}));
}

sub Forest::Hypertag::pretty_print {
  my $self=shift;
  my $ht = $self->{ht};
  return $ht ? $ht->pretty_print(2,'ht') : ();
}

1;
__END__

=head1 NAME

Forest::XML::Writer

=head1 DESCRIPTION

=head1 SYNOPSIS

  use Forest::XML::Writer;

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

