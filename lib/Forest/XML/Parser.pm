# $Id$
package Forest::XML::Parser;

require 5.005_62;
use strict;
use warnings;
use Forest;
use XML::Parser;

our @ISA = qw{XML::Parser};

our $forest;
our $htid = "ht0000";

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = $class->SUPER::new(Pkg=>__PACKAGE__,
                                  Style=>'Subs',
                                  @_);
    $self->setHandlers(Final => \&Final,
                       Init => \&Init,
                       Char => \&Char
                       );
    $htid="ht000";
    return $self;
}

sub parse {
  my $expat = __PACKAGE__->new();
  return $expat->SUPER::parse(@_);
}

sub Char {
  my $expat = shift;
  my $text = shift;
  $text =~ s/^\s+//o;
  $text =~ s/\s+$//o;
  $expat->{Current}{$expat->{'Where'}}=$text unless ($text  =~ /^\s*$/);
}

sub Final {
  my $expat = shift;
  delete $expat->{Current};
  delete $expat->{Where};
  delete $expat->{Stack};
  ## normalize anchor node by adding missing info on tree
  ## this info is somewhat redundondant with the tree argument on derivations
  ## but seems to be needed to emit dependencies
  my $forest = $expat->{Forest};
  foreach my $head (keys %{$forest->{op}}) {
    my $op = $forest->{op}{$head};
    next if ($op->{type} eq 'anchor' || $op->{type} eq 'lexical' || $op->{type} eq 'struct');
    foreach my $deriv (@{ $op->{derivations} }) {
      my $tree = $deriv->{tree};
      foreach my $label (keys %{$deriv->{nodes}}) {
	foreach my $target (@{$deriv->{nodes}{$label}}) {
	  my $t = $forest->{op}{$target};
	  $t->{tree} ||= $tree if ($t->{type} eq 'anchor');
	}
      }
    }
  }
  return $forest;
}

sub Init {
    my $expat = shift;
    $expat->{Forest} = Forest->new( {'op'=> {}, 'nb' => 0} );
 }

sub forest {
    my $expat = shift;
    my $tag = shift;
    my %attr = @_;
    $expat->{Forest}{'parser'} = $attr{'parser'} if (defined $attr{'parser'});
##    $expat->push($expat->{Forest});
    $expat->push([]);
}

sub forest_ {
  my $expat = shift;
  $expat->pop();
}

sub sentence {
  my $expat = shift;
    $expat->{'Where'} = 'sentence';
}

sub sentence_ {
    my $expat = shift;
    undef $expat->{Where};
}

sub op {
    my $expat = shift;
    my $tag = shift;
    my %attr = @_;
    my $op = Forest::Op->new('id'=>$attr{'id'},
			     'span'=>[ split(/\s+/,$attr{'span'}) ],
			     'type' => $attr{'type'},
			     'derivations' => []);
    $op->{cat} = $attr{cat} if (exists $attr{cat});
    $op->{lemma} = $attr{lemma} if (exists $attr{lemma});
    $op->{lex} = $attr{lex} if (exists $attr{lex});
    $expat->{'Forest'}{'op'}{$op->{'id'}}=$op;
    $expat->{'Forest'}{'nb'} = $op->{'id'} if ($op->{id} > $expat->{'Forest'}{'nb'});
    push(@{$expat->{Current}},$op->{id});
    $expat->push($op);
}

sub op_ {
    shift->pop();
}

sub opref {
    my $expat = shift;
    my $tag = shift;
    my %attr = @_;
    my $id = $attr{ref};
    push(@{$expat->{Current}},$id);
}


sub narg {
    my $expat = shift;
    shift;
    my %attr = @_;
    my $node = $expat->{Current};
    $expat->{Where} = $attr{'type'};
}

sub narg_ {
    my $expat = shift;
    undef $expat->{Where};
}

sub deriv {
    my $expat = shift;
    my $tag = shift;
    my %attr = @_;
    my $anchor = $attr{'node'} || '<>';
    my $deriv = Forest::Deriv->new( nodes => {}  );
    $deriv->{tree} = $attr{tree} if (exists $attr{tree});
    push(@{$expat->{Current}{'derivations'}},$deriv);
    $expat->push($deriv);
}

sub deriv_ {
    shift->pop;
}


## Do not correctly read recursive feature structure for HT
## ***Need to investigate !
sub hypertag {
  my $expat = shift;
  shift;
  my %attr = @_;
  my $deriv = $expat->{Current};
  my $hid = $htid++;
  my $ht = Forest::Hypertag->new( id => $hid,
				  derivs => [],
				  tree => ""
				);
  $expat->{Forest}{hypertag}{$hid} = $ht;
  $deriv->{ht} = $hid;
  $expat->push($ht);
  $expat->{Where} = 'ht';
}

sub hypertag_ {
  my $expat = shift;
  $expat->pop;
  undef $expat->{Where};
}

sub node {
  my $expat = shift;
  my $tag = shift;
  my %attr = @_;
  my $name = $attr{name};
  my $l = $expat->{Current}{nodes}{$name} ||= [];
  $expat->push($l);
}

sub node_ {
  shift->pop;
}

## Handling Feature Structures

sub plus {
    my $self = shift;
    my $plus = Forest::Plus->new();
    push( @{ $self->{Current}{'value'} },$plus);
}

sub minus {
    my $self = shift;
    my $minus = Forest::Minus->new();
    push( @{ $self->{Current}{'value'} },$minus);
}

sub fs {
    my $self = shift;
    my $feature = Forest::Feature->new();
    my $where = $self->{Where} || 'feature';
    $feature->{Save_Where} = $self->push_val($feature,$where);
    $self->push($feature);
    $self->{Where} = 'val';
}

sub fs_ {
  my $self = shift;
  $self->{Where} = $self->{Current}{Save_Where};
  delete $self->{Current}{Save_Where};
  $self->pop();
}

sub f {
    my $self = shift;
    my $tag = shift;
    my %attr = @_;
    my $comp = Forest::Comp->new( { comp => $attr{name}, value =>[] } );
    $self->{Current}{'f'}{$comp->{'comp'}} = $comp;
    $self->push($comp);
}

sub f_ {
    shift->pop;
}

sub val {
    my $self = shift;
    my $tag = shift;
    my $val = Forest::Val->new();
    push( @{ $self->{Current}{'value'} },$val);
    $self->push($val);
}

sub val_ {
    shift->pop;
}

sub link {
    my $self = shift;
    my $tag = shift;
    my %attr = @_;
    $self->{Current}{'id'}=$attr{'id'};
}


1;

__END__

=head1 NAME

Forest::XML::Parser

=head1 DESCRIPTION

=head1 SYNOPSIS

  use Forest::XML::Parser;

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

