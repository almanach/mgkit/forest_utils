# $Id$
package Forest::Dependency::Reader;

require 5.005;
use strict;
use warnings;
use Forest;
use XML::Parser;

our @ISA = qw{XML::Parser};

our $forest;
our %tmp;
our @entries;


sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = $class->SUPER::new(Pkg=>__PACKAGE__,
				Style=>'Subs',
				@_);
  $self->setHandlers(Final => \&Final,
		     Init => \&Init,
		     Char => \&Char
		    );
  return $self;
}


sub parse {
  my $self = __PACKAGE__->new;
  return $self->SUPER::parse(@_);
}

sub Char {
  my $self = shift;
  my $text = shift;
  $text =~ s/^\s+//o;
  $text =~ s/\s+$//o;
  ($text  !~ /^\s*$/ && defined $self->{Where}) and $self->{Current}{$self->{'Where'}}=$text;
}

sub Final {
  my $self = shift;
  delete $self->{Current};
  delete $self->{Where};
  delete $self->{Stack};
  $self->{Forest}{sid} ||= "E1";
  foreach my $oid (keys %{$self->{Forest}{op}}) {
    my $op = $self->{Forest}{op}{$oid};
    unless (exists $op->{type}) {
      ## Desambiguation on robust parses may return
      ## nodes not belonging to any derivation
      ## => pseudo derivations are therefore introduced
      ## maybe the problem should better be solved during the desambiguation (easyforest)
      printf STDERR "Filling incomplete op $oid lex=$op->{lex} span=@{$op->{span}}\n";
      $op->{type} = 'lexical';
      $op->{tree} = 'lexical';
      my $id = ++$self->{Forest}{nb};
      $id = "pseudo$id";
      $self->{Forest}{op}{$id} = 
	Forest::Op->new( id => $id,
			 cat => 'pseudo',
			 lex => '',
			 token => '',
			 lemma => '',
			 lemmaid => '',
			 type => 'subst',
			 span => $op->{span},
			 derivations => [ Forest::Deriv->new( tree => 'pseudo',
							      nodes => { void => [$oid] }
							    )
					]
		       );
    }
  }
  return $self->{Forest};
}

sub Init {
  my $self = shift;
  $self->{Forest} = Forest->new( {'op'=> {}, hypertag => {}, 'nb' => 0} );
  $self->{cluster} = {};
  $self->{deriv} = {};
  $self->{op} = {};
  $self->{hypertag} = {};
}


sub dependencies {
  my $self = shift;
}

sub dependencies_ {
}

=head1 METHODS

=head2 cluster

A cluster provides information about a portion of the input sentence 
precisely between positions left and right

=cut

sub cluster {
  my $self = shift;
  my $tag = shift;
  my %attr = @_;
  my $id = $attr{id};
  if ((!exists $self->{Forest}{sid}) 
      && $id =~ /^E(\d+(?:\.\d+)?)/) {
    $self->{Forest}{sid} = $1;
  }
  $self->{cluster}{$id}
    = { lex => $attr{lex},
	token => $attr{token} || $attr{lex},
	span =>  [$attr{left},$attr{right}] };
}

=head2 node

Node elements are used to denote anchors and other lexicals
nodes are then covered by maximal non terminals 'op'
A node refers a cluster

=cut

sub node {
  my $self = shift;
  my $tag = shift;
  my %attr = @_;
  my $id = $attr{id};
  my $cluster = $self->{cluster}{$attr{cluster}};
  my $lex = $cluster->{lex};
  my $oid;
  my $tree = $attr{tree};
  unless ($lex eq '') {
    ## the node is not a virtual one ($lex eq '')
    ## but is not necessarily an anchor node
    ## it may also be a lexical or epsilon
    $oid = $self->{op}{$id} ||= ++$self->{Forest}{nb};
##    print "Map dep op=$id to forest op=$self->{op}{$id}\n";
    my $op = $self->{Forest}{op}{$oid} 
      ||= Forest::Op->new( id => $oid,
			   derivations => [],
			   cat => $attr{cat},
			   lex => $lex,
			   token => $attr{form} || $cluster->{token},
			   span => $cluster->{span}
			 );
    (exists $attr{lemma}) and $op->{lemma} = $attr{lemma};
    (exists $attr{lemmaid}) and $op->{lemmaid} = $attr{lemmaid};
    ##    print STDERR "Register node $oid: span=@{$op->{span}} les=$op->{lex} cat=$op->{cat}\n"
  }
  if (exists $attr{deriv}) {
    my $op;
    if (defined $oid) {		
      # A node represents an anchor if it is has a deriv, a tree and a lex
      $op = $self->{Forest}{op}{$oid};
      $op->{type} = 'anchor';
      $op->{tree} = $tree;
      $self->{Forest}{anchor}{$oid} = 1;
      ##      print STDERR "Register anchor $oid\n";
    }
    foreach my $did (split(/\s+/,$attr{deriv})) {
      ## each deriv is associated to a Forest::Deriv whose
      ## tree is $tree and anchor is $op
      ## The derive included in some Forest::Op whose cat is $xcat
      $did =~ s/_\d+$//;
      (exists $self->{deriv}{$did}) and next;
      my $deriv = $self->{deriv}{$did}
	= Forest::Deriv->new( tree => $tree,
			      nodes => {}
			    );
      $op and $op->{did} ||= $did; # store first derivation for op, to be used for some rerooting cases
      ($lex eq '') or $deriv->{nodes}{$attr{cat}} = [$oid];
    }
  } 
}

=head2 edge

Edges relate two nodes and provide additionnal info

=cut

sub edge {
  my $self = shift;
  my $tag = shift;
  my %attr = @_;

  my $sid = $self->{op}{$attr{source}};
  my $tid = $self->{op}{$attr{target}};

#  $sid or warn("empty source op sid=$attr{source}");
#  $tid or warn("empty source op sid=$attr{target}");
  
  my $edge = { source => $sid,
	       target => $tid,
	       label => $attr{label},
	       type => $attr{type},
	       sid => $attr{source},
	       tid => $attr{target},
	       eid => $attr{id}
	     };
  (exists $attr{w}) and $edge->{weight} = $attr{w};
  $self->push($edge);
}

sub edge_ {
  shift->pop();
}

=head2 deriv

derivs are embedded within edges and indicate the op
covering the target node
plus a deriv id

=cut

sub deriv {
  my $self = shift;
  my $tag = shift;
  my %attr = @_;
  my $edge = $self->{Current};
  my $eid = $edge->{eid};
  my $type = $edge->{type};
  foreach my $did ($attr{name} || split(/\s+/,$attr{names}) ){
    ## the use of the 'name' attribute is deprecated in favor of 'names'
    ##  my $did = $attr{name};
    $did =~ s/_\d+$//;
    my $deriv = $self->{deriv}{$did}; 
    my $tid;
    my $tmpid;
    #   print "Edge $edge->{source}/$edge->{sid} $edge->{target}/$edge->{tid} $type did=$did sop=$attr{source_op} top=$attr{target_op}\n";
    if ($type eq 'secondary') {
      $tid = $edge->{target};
      if (defined $tid && exists $self->{Forest}{anchor}{$tid}) {
	$tmpid = $attr{target_op};
	$tid = $self->{op}{$tmpid} ||= ++$self->{Forest}{nb};
      }
    } elsif ($type eq 'lexical' || $type eq 'epsilon') {
      $tid = $edge->{target};
      $tmpid = $edge->{tid};
      $self->{Forest}{op}{$tid}{type} ||= $type;
    } else {
      $tmpid = $attr{target_op};
      $tid = $self->{op}{$tmpid} ||= ++$self->{Forest}{nb};
      my $target_op = $self->{Forest}{op}{$tid}
	||= Forest::Op->new( id => $tid,
			     derivations => [],
			   );
      $target_op->{type}=$type;
      ##      print "\tMap target_op=$tmpid to forest op=$tid\n";
      if (exists $attr{reroot_source} && defined $edge->{target}) {
	my $target_anchor_id = $edge->{target};
#	print "reroot eid=$eid did=$did source=$edge->{source} target=$edge->{target} top=$tid target_anchor_id=$target_anchor_id\n";
	if ($deriv
	    && exists $deriv->{head}
	    && $deriv->{head} == $target_anchor_id
	   ) {
#	  print "*** deriv=$did.$eid n$tid is both head and target; also $did has two possible heads, n$tid and n$edge->{source}\n";
	  ## we need to add an extra derivation attached to the source node
	  my $source_anchor = $self->{Forest}{op}{$edge->{source}};
	  if (my $altdid = $source_anchor->{did}) {
	    $deriv = $self->{deriv}{$altdid};
	    $tid = $self->{op}{$attr{source_op}} ||= ++$self->{Forest}{nb};
	    $self->{Forest}{op}{$tid} 
	      ||=  Forest::Op->new( id => $tid,
				    derivations => [],
				    type => $edge->{type}
				  );
#	    print "*** $eid is added to alternative derivation $altdid\n";
	  }
	} elsif (1
		 && $target_anchor_id  
		 && !exists $self->{Forest}{op}{$target_anchor_id}{tree}
		) {
	  my $target_anchor = $self->{Forest}{op}{$target_anchor_id};
	  ## we have a reroot via adj or subst to a target that is a lexical node
	  ## we have no derivation for $target
	  ## we need to forge a derivation
#	  print "reroot missing tree eid=$eid did=$did target_op=$tid span=@{$target_anchor->{span}}\n";
	  $target_anchor->{type} = 'anchor';
	  $target_op->{span} = $target_anchor->{span};
	  my $tderiv = Forest::Deriv->new( tree => 'lexical', nodes => {} );
	  push(@{$tderiv->{nodes}{$target_anchor->{cat}}},$target_anchor_id);
	  push(@{$target_op->{derivations}}, $tderiv );
	  #	  $target_op->{added_derivs}{$did} = 1;
	  $self->{opredirect}{$attr{source_op}} = $target_op;
	}
      }
    }
    if (exists $attr{reroot_source}) {
#      print "reroot add edge eid=$eid did=$did sid=$edge->{sid} tid=$edge->{tid}\n";
      if (!$deriv) {
	## the source node has no attached derivs (otherwise $deriv would have been created)
	## the reason may be that it is a lexical node used as an anchor because of rerooting
#	print "here eid=$eid\n";
	my $anchorid =  $edge->{source};
	$anchorid or warn("empty source in reroot_source sid=$edge->{sid}");
	my $anchor = $self->{Forest}{op}{$anchorid}
	  ||=  Forest::Op->new( id => $anchorid,
				derivations => [],
			      );
	$deriv = $self->{deriv}{$did} 
	  ||= Forest::Deriv->new( tree => $anchor->{tree} || 'lexical',
				  nodes => {}
				);
	$anchor->{type} = 'anchor';
#	print "set head for $did at $anchorid label=$anchor->{cat}\n";
	push(@{$deriv->{nodes}{$anchor->{cat}}},$anchorid);
	$deriv->{head} = $anchorid;
      }
#      print "did=$did label=$edge->{label} target=$edge->{target} source=$edge->{source} reroot_source=$attr{reroot_source}\n";
      #      $deriv->{reroot}{$edge->{label}}{$edge->{target} || ""} = $edge->{source};
      $deriv->{reroot}{$edge->{label}}{$tid} = $edge->{source}; 
    }
    if (exists $attr{secondary}) {
      #      print "Register secondary on $edge->{label}\n";
      my $secondary = $attr{secondary};
      my $label2 = $attr{secondary_label} || $edge->{label};
      my $oid2 = $self->{op}{$secondary} ||= ++$self->{Forest}{nb};
      push(@{$deriv->{secondary}{$tid}},[$secondary,$oid2,$label2,1]);
    } elsif ($type eq 'secondary') {
      my $label2 = $edge->{label};
      ##      my $oid2 = $self->{op}{$attr{source_op}} ||= ++$self->{Forest}{nb};
      my $oid2= $edge->{source} || ($self->{op}{$attr{source_op}} ||= ++ $self->{Forest}{nb}) ;
      my $secondary = $oid2;
      my $direct = 1;
      my $tid2 = $tid;
      if (exists $attr{reverse} && $attr{reverse}) {
	$tid2 = $edge->{source};
	if (exists $self->{Forest}{anchor}{$tid2}) {
	  my $tmpid = $attr{source_op};
	  $tid2 = $self->{op}{$tmpid} ||= ++ $self->{Forest}{nb};
	}
	$oid2 = $edge->{target} || ($self->{op}{$attr{target_op}} ||= ++ $self->{Forest}{nb}) ;
	$secondary = $oid2;
	$direct = 0;
      }
      if (defined $tid2) {
	unless (grep {$secondary eq $_->[0]
			&& $label2 eq $_->[2]
			&& $oid2 eq $_->[1]} @{$deriv->{secondary}{$tid2}}) {
#	  print "# secondary tid=$tid nid2=$secondary oid2=$oid2 source_op=$attr{source_op} label2=$label2 direct=$direct\n";
	  push(@{$deriv->{secondary}{$tid2}},[$secondary,$oid2,$label2,$direct]);
	}
      }
    }
    ##    print STDERR "Reader register edge $edge->{sid}:$edge->{source} -[$edge->{label}:$edge->{type}]-> $edge->{tid}:$edge->{target} [max=$tmpid:$tid] $attr{reroot_source}\n";
      #    if ($deriv && !(exists $deriv->{head} && $deriv->{head} == $tid)) {
      #    print "add edge did=$did label=$edge->{label} $tid\n";
    ($type eq 'secondary') or push(@{$deriv->{nodes}{$edge->{label}}},$tid);
#    }
    (exists $edge->{weight}) and $deriv->{weights}{$edge->{label}}{$tid} = $edge->{weight};
  }
}

=head2 op

op elements are used for maximal non terminals 
information provided by the deriv attribute is not used here
because it is redundant with the info provided by the deriv element
within edges

=cut

sub op {			
  my $self = shift;
  my $tag = shift;
  my %attr = @_;

  my $id = $attr{id};
  my $oid = $self->{op}{$id} ||= ++$self->{Forest}{nb};
  my $op = $self->{Forest}{op}{$oid} 
    ||= Forest::Op->new( id => $oid,
			 derivations => [],
			 type => 'subst'
		       );
  $op->{span} = [ split(/\s+/,$attr{span}) ];
  $op->{cat} = $attr{cat};

  if (exists $attr{deriv}) {
    my %seen=();
    (exists $op->{added_derivs}) and %seen = %{ $op->{added_derivs} };
    foreach my $did (split(/\s+/,$attr{deriv})) {
      $did =~ s/_\d+$//;
      next if (exists $seen{$did});
      $seen{$did} = 1;
      my $deriv = $self->{deriv}{$did};
##      print "add deriv $did to op $oid\n";
      push(@{$op->{derivations}},$deriv);
      if (0) {
	print "deriv $did op $id\n";
	foreach my $label (keys %{$deriv->{nodes}}) {
	  print "\t$label @{$deriv->{nodes}{$label}}\n";
	}
	foreach my $label (keys %{$deriv->{reroot}}) {
	  foreach my $target (keys %{$deriv->{reroot}{$label}}) {
	    print "\treroot $label $target $deriv->{reroot}{$label}{$target}\n";
	  }
	}
      }
    }
  }
##  print "OP depid=$id forestid=$oid\n";
  if (my $redirect = $self->{opredirect}{$id}) {
    $redirect->{span} = $op->{span};
    $redirect->{cat} = $op->{cat};
  }
  $self->push($op);
}

sub op_ {
  shift->pop;
}

=head2 hypertag

hypertag elements are used to provide the feature structure
information attached to derivations, and resulting from the
unification of the hypertags of the trees and anchors.

=cut

sub hypertag {			
  my $self = shift;
  my $tag = shift;
  my %attr = @_;
  $attr{derivs} = $attr{derivs} || "";
  my $id = $attr{id};

  my @derivs = split(/\s+/,$attr{derivs});
  ##  my $oid = $attr{op};

  ##  print "Register ht $id for derivs @derivs\n";

  my $hypertag = $self->{Forest}{hypertag}{$id} 
    ||= Forest::Hypertag->new( id => $id,
			       derivs => [@derivs]
			     );
  
  ## All derivs under an hypertag share the same base derivation
  ## my $did = $derivs[0];

  ## we are assuming that everything is OK
  ## ie that the derivs have already been created !
#  $did = $did || "";
#  my $deriv = $self->{deriv}{$did};

  foreach my $did (@derivs) {
    #    my $d = $self->{Forest}{deriv}{$did};
    my $d = $self->{deriv}{$did};
    $hypertag->{tree} = $d->{tree};
    $d->{ht} = $id;
  }

  ##  $self->push($deriv);
  $self->push($hypertag);
  $self->{Where} = 'ht';
}

sub hypertag_ {
  my $self = shift;
  $self->{Current}{ht}{type} = 'ht' if (defined $self->{Current}{ht});
  undef $self->{Where};
  $self->pop;
}

######################################################################

sub narg {
    my $self = shift;
    shift;
    my %attr = @_;
    my $node = $self->{Current};
    $self->{Where} = $attr{'type'};
}

sub narg_ {
    my $self = shift;
    undef $self->{Where};
}

## Handling Feature Structures

sub plus {
    my $self = shift;
    my $plus = Forest::Plus->new();
    push( @{ $self->{Current}{'value'} },$plus);
}

sub minus {
    my $self = shift;
    my $minus = Forest::Minus->new();
    push( @{ $self->{Current}{'value'} },$minus);
}

sub fs {
    my $self = shift;
    my $feature = Forest::Feature->new();
    my $where = $self->{Where} || 'feature';
    $feature->{Save_Where} = $self->push_val($feature,$where);
    $self->push($feature);
    $self->{Where} = 'val';
}

sub fs_ {
  my $self = shift;
  $self->{Where} = $self->{Current}{Save_Where};
  delete $self->{Current}{Save_Where};
  $self->pop();
}

sub f {
    my $self = shift;
    my $tag = shift;
    my %attr = @_;
    my $comp = Forest::Comp->new( { comp => $attr{name}, value =>[] } );
    $self->{Current}{'f'}{$comp->{'comp'}} = $comp;
    $self->push($comp);
}

sub f_ {
    shift->pop;
}

sub val {
    my $self = shift;
    my $tag = shift;
    my $val = Forest::Val->new();
    push( @{ $self->{Current}{'value'} },$val);
    $self->push($val);
}

sub val_ {
    shift->pop;
}

sub link {
    my $self = shift;
    my $tag = shift;
    my %attr = @_;
    $self->{Current}{'id'}=$attr{'id'};
}

## utilities
package XML::Parser::Expat;
use Scalar::Util qw{reftype};

sub push {
    my ($self,$obj) = @_;
    CORE::push @{ $self->{Stack} }, $self->{Current};
    $self->{Current} = $obj;
}

sub pop {
    my $self = shift;
    $self->{Current} = CORE::pop @{ $self->{Stack} };
}

sub push_val {
  my $self = shift;
  my $val = shift;
  my $where = $self->{Where} || shift || 'value';
  if (defined $self->{Current}{$where}
      && reftype($self->{Current}{$where}) eq "ARRAY") {
    CORE::push(@{$self->{Current}{$where}},$val);
  } else {
    $self->{Current}{$where} = $val;
  }
  return $where;
}


1;

__END__

=head1 NAME

Forest::Dependency::Reader - Reading XML DEP files and build an
internal representation of shared forest.

=head1 SYNOPSIS

  use Forest::Dependency::Reader;

=head1 DESCRIPTION

=head2 EXPORT

None by default.

=head1 SEE ALSO

perl(1).

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

