#$Id$
package Forest::Dependency::Writer;

require 5.005_62;
use strict;
use warnings;
use Forest;
use Forest::XML::Writer;
use Scalar::Util qw{blessed};
use List::Util qw{reduce min max};

our $forest;
our $dependency;
our $pseudoop;

our %color = ( 'subst'   => 'LightSkyBlue1',
	       'adj'     => 'IndianRed3',
	       'anchor'  => 'yellow',
	       'lexical' => 'Plum1',
	       'epsilon' => 'green',
	       'secondary' => 'SlateGray'
	     );

our %trees;

our $dcounter;			# to assign id to derivations
our $htid;
our $nid;
our $eid;

our $eskelcount;

our %derivations;

sub cluster {
    my ($l, $r) = @_;
    return "cluster$l"."_"."$r";
}

sub node {
  my $node  = shift;
  my $label = cluster($node->{pos}[0], $node->{pos}[1])."_$node->{cat}_$node->{lemmaid}_".norm_tree(treename($node->{tree}));
  $label =~ s/\W//og;
  $label =~ s/_+/_/og;
  return $label;
}

sub treename {
  my $treename = shift;
  return (split(/\s+/,$treename))[0];
}

sub Forest::cluster_loop {
  my $self = shift;
  my $cluster_handler = shift;
   foreach my $posa (sort {$a <=> $b} keys %{$dependency}) {
     foreach my $posb (sort {$a <=> $b} keys %{$dependency->{$posa}}) {
       $cluster_handler->($self, $posa, $posb, $dependency->{$posa}{$posb}, @_);
     }
   }
}

sub Forest::node_loop {
  my ($self, $posa, $posb, $nodes, $node_handler, @l) = @_;
  foreach my $lemmas (values %{$nodes->{tree}}) {
    foreach my $trees (values %$lemmas) {
      foreach my $target (values %$trees) {
	$node_handler->($self, $target,@l);
      }
    }
  }
}

sub Forest::edge_loop {
  my ($self,$target,$edge_handler,@l) = @_;
  foreach my $label (keys %{$target->{label}}) {
    foreach my $edge (@{$target->{label}{$label}}) {
      $edge_handler->($self,$target,$edge,$label,@l);
    }
  }
}

sub Forest::dependency_normalize {
    my $self = shift;
    my $nb   = $self->{'nb'};
    $forest  = $self;
    $dependency = {};
    %trees = ();
    $pseudoop={};

    # First pass
    foreach my $xop (values %{$self->{'op'}}) {
      $xop->dependency();
    }

    # Second pass: emiting
    foreach my $pos (sort {$a <=> $b} keys %{$dependency}) {
	foreach my $posb (sort {$a <=> $b} keys %{$dependency->{$pos}}) {
	    my $lex   = $dependency->{$pos}{$posb}{'lex'};
	    my @trees = keys %{$dependency->{$pos}{$posb}{'tree'}};
	    foreach my $tree (sort @trees) {
		my $cat      = $trees{$tree}{'cat'};
		my $node     = $trees{$tree}{'node'};
		my $edges    = $dependency->{$pos}{$posb}{'tree'}{$tree};
		my $treename = treename($tree);
		foreach my $label (sort keys %{$edges}) {
		    foreach my $target (@{$edges->{$label}}) {
			my $tpos  = $target->{'pos'}[0];
			my $tposb = $target->{'pos'}[1];
			my $ttree = $target->{'tree'};
			my $ttreename = treename($ttree);
			my $tlex  = $dependency->{$tpos}{$tposb}{'lex'};
			my $tcat  = $trees{$ttree}{'cat'};
			my $tnode = $trees{$ttree}{'node'};
			print "$target->{'type'}\t{$pos $posb $lex $treename $node $cat}\t{$tpos $tposb $tlex $ttreename $tnode $tcat}\t$label\n";
		    }
		}
	    }
	}
    }
}

sub tree_emit {
  my ($node,$grammar) = @_;
  my $treename = treename($node->{tree});
  my $xnode = node($node);
  my $cat = $node->{cat};
  my $url = (defined $grammar && defined $treename) ? "URL=\"$grammar?id=$treename\"" : "";
  my $lemma = $node->{lemmaid} || '_';
  $treename ||= "";
  my $deriv = 0;
  foreach my $dskel (@{$node->{deriv}}) {
    $deriv += $dskel->count;
  }
  my $label = "$lemma:$cat:$treename ($deriv)";
  $label =~ s/"/\\"/og;
  return <<EOF;
$xnode [label="$label", $url]
EOF
}

sub Forest::collect_dependency {
  my $self = shift;

  ## try to reuse already computed dependencies
  ## ** WARNING ** should check it is enough to store only $dependency

  if (exists $self->{dependency}) {
    $dependency = $self->{dependency}{dep};
    %trees = %{$self->{dependency}{trees}};
    $forest = $self;
  } else {
    
    $dependency = {};
    $forest = $self;
    %trees = ();
    %derivations = ();
    
    ##  $self->{pseudoop} = {};
    $pseudoop={};
    
    ##  $dcounter="d0000";
    ## my $sid = $self->{sid};
    $dcounter="d000000";
    $htid = "ht0001";
    $nid   = "n001";
    $eid   = "e0001";
    $eskelcount = 0;

    $_->dependency() foreach (values %{$self->{'op'}});
    
    $self->{dependency} = { dep => $dependency,
			    trees => \%trees
			  };
  }
}

sub xml_escape {
  my $s = shift;
  $s =~ s/\&/&amp;/og;
  $s =~ s/</&lt;/og;
  $s =~ s/>/&gt;/og;
  $s =~ s/"/&quot;/og;
  return $s;
}

sub xml_derivs {
  my $derivs = shift;
  my @derivs = ();
  my $last;
  my $current;
  my $orig;
  foreach my $d (sort {$a cmp $b} @$derivs) {
    my ($xd) = $d =~ /d0*(\d+)/o;
    if (!defined $orig) {
      $orig = $d;
      $current = $xd;
      $last = $xd;
    } elsif ($xd == ($last+1)) {
      $last = $xd;
    } elsif ($last == $current) {
      push(@derivs,$orig);
      $last = $xd;
      $current = $xd;
      $orig = $d;
    } else {
      push(@derivs,join(':',$orig,($last-$current)));
      $last = $xd;
      $current = $xd;
      $orig = $d;
    }
  }
  if (defined $orig) {
    push(@derivs, ($last == $current) ? $orig : join(':',$orig,($last-$current)));
  }
  return join(' ',@derivs);
}

sub Forest::xmldep {
    my $self = shift;
    my $sid = $self->{sid};

    if (0) {   
      my @ht = keys %{$self->{hypertag}};
      print "ALL HT @ht\n";
    }

    # First pass: collecting dependencies
    $self->collect_dependency;
##    return;

    my %nodes = ();
    my $GEN   = new MyGenerator('encoding' => 'ISO-8859-1', 
				   ## :pretty = pretty + escaping of illegal XML chars in attributes
				   ## slower than just pretty but useful !
				   ## reverted to pretty + use of own function xml_escape
				  ## 'pretty' => 1 
				  );

    $Forest::XML::Writer::GEN=$GEN;

    my @clusters = ();
    my @nodes    = ();
    my @edges    = ();
    my @op       = ();
    my @ht       = ();
##    my %nodes    = ();
    my %clusters = ();

    my $xmlcluster = sub {
      my ($posa,$posb) = @_;
      my $cid = "${sid}c_".$posa."_".$posb;
      return if (exists $clusters{$cid});
      $clusters{$cid} = 1;
      my $cluster = $dependency->{$posa}{$posb};
      my $token = $cluster->{lex} || "";
      $token =~ s/E\d+(\.\d+)?F\d+\|//go;
      my $ltoken = lc($token);
      if ($token ne $ltoken) {
	## normalize token to lowercase if there is a node with such a form.
	foreach my $tok (@{$cluster->{token}}) {
	  $token = $ltoken, last if ($tok eq $ltoken);
	}
      }
      $GEN->cluster({ id    => $cid,
		      left  => $posa,
		      right => $posb,
		      lex   => xml_escape($cluster->{lex}) || "",
		      ## deprecated: to be removed in some future
#		      token => xml_escape($cluster->{token}) || ""
		      token => xml_escape($token),
		    })->emit("  ");
    };

    my $xmlnode = sub {
      my $node = shift;
      my ($posa,$posb)  = @{$node->{pos}};
      my $cat   = $node->{cat};
      my $lemma = $node->{lemma};
      my $lemmaid = $node->{lemmaid};
      my $tree  = $node->{tree};
      my $token = $node->{token};
      my $id = $node->{nid};
      
      unless (exists $nodes{$id}) {
	$nodes{$id} = 1;
	$xmlcluster->($posa,$posb);
	my $cluster = "${sid}c_".$posa."_".$posb;
	my %attr = ( id      =>  $id,
		     cluster => $cluster,
		     tree    => $tree || 'void',
		     cat     => $cat,
		     lemma   => xml_escape($lemma),
		     lemmaid   => xml_escape($lemmaid),
		     form => xml_escape($token)
		   );
	my $deriv = $node->{deriv};
	if ($deriv) {
	  $nodes{$id} = $attr{deriv} = xml_derivs([map {$_->dids} @$deriv]);
	}
	$attr{xcat}  = $trees{$tree}{cat} if ($tree);
	$GEN->node(\%attr)->emit("  ");
      }
   };
    
    # Second pass: build xml
    my $edge_handler = 
      sub { my ($self,$target,$edge,$label) = @_;
	    my $source = $edge->{source};
	    $xmlnode->($source);
	    ##			     print STDERR "xml dep edge $ida->$idb label=$label type=$target->{type}\n";
	    my %tmp = ();
	    foreach my $dskel (@{$edge->{deriv}}) {
	      my $dids = $dskel->[0]{prefix};
	      my ($source_op,$target) = @{$dskel->[3]};
	      my $target_op = $target->{id};
	      my $k = "$target_op $source_op";
	      unless (exists $tmp{$k}) {
		my $info = $tmp{$k} = { span => "@{$target->{span}}", 
					target_op => "${sid}o$target_op",
					source_op => "${sid}o$source_op",
					names => { $dids => 1}
				      };
		my $reroot = $dskel->[2];
#		$reroot and print "CASE 1 reroot $reroot->{nid}\n";
		$reroot and $info->{reroot_source} = $reroot->{nid};
	      } else {
		$tmp{$k}{names}{$dids} = 1;
		if (my $reroot = $dskel->[2]) {
#		  print "CASE 2 reroot $reroot->{nid}\n";
		  if (1 || $tmp{$k}{reroot_source}) {
		    $tmp{$k}{reroot_source} .= " $reroot->{nid}";
		  } else {
		    $tmp{$k}{reroot_source} = $reroot->{nid}; 
		  }
		}
	      }
	      defined $dskel->weight and $edge->{weight} = $dskel->weight;
	    }
	    my @k = keys %tmp;
	    if (scalar(@k) == 1) {
	      ## remove names attr if all derivs from source node go through the edge
	      ## i.e. when the edge is a mandatory one !
	      my $k = $k[0];
	      my $v = $tmp{$k};
	      if ($v->{names} eq $nodes{$source->{nid}}) {
		delete $v->{names};
		$v->{mandatory} = 1;
	      }
	    }
	    my $attrs = {target => $target->{nid},
			 source => $source->{nid},
			 id     => "${sid}".$eid++,
			 type   => $edge->{type},
			 label  => $label,
			};
	    exists $edge->{weight} and $attrs->{w} = $edge->{weight};
	    foreach my $deriv (values %tmp) {
	      $deriv->{names} = xml_derivs([keys %{$deriv->{names}}]);
	    }
	    $GEN->edge($attrs,
		       map($GEN->deriv($_),values %tmp)
		      )->emit("  ");
	  };

    my $node_handler = 
      sub { my ($self,$target) = @_;
	    $xmlnode->($target);
	    $self->edge_loop($target,$edge_handler);
	  };

    my $cluster_handler = 
      sub { my ($self,$posa,$posb,$nodes) = @_;
	    ##				print STDERR "CLUSTER $posa $posb\n";
	    $xmlcluster->($posa,$posb);
	    $self->node_loop( $posa, $posb, $nodes, $node_handler );
	  };

    # Third pass: emit
#    print $GEN->xmldecl;
    my $attrs='';

    $attrs  .= " grammar=\"$self->{grammar}\""  if (defined $self->{grammar});
    $attrs .= " sentence=\"".xml_escape($self->{sentence})."\"" if (defined $self->{sentence});
    $attrs  .= " mode=\"$self->{mode}\"" if (defined $self->{mode});

    print <<EOF;
<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>
<dependencies$attrs>
EOF

    $self->cluster_loop( $cluster_handler );

    $self->xmldep_op($GEN);
    if (0) {
      my @ht = keys %{$self->{hypertag}};
      print "ALL HT @ht\n";
    }
    foreach (values %{$self->{hypertag}}) {
      $_->xmldep_ht($GEN);
    }

    print <<EOF;
</dependencies>
EOF

    return 1;
  }

sub lp_emit {
  my ($type,$features) = @_;
  $features = join(",\n",map {"\t$_ => $features->{$_}"} keys %$features);
  print "x(${type}{\n$features\n\t}).\n\n";
}


sub lp_emit_aux {
  my ($type,$features) = @_;
  $features = join(",\n",map {"\t\t$_ => $features->{$_}"} keys %$features);
  return "${type}{\n$features\n\t}";
}

sub lp_emit_list {
  return '['.join(",\n",map {lp_shift_aux($_)} @_).']';
}

sub lp_shift_aux {
  my $s = shift;
  $s =~ s/^/\t/msog;
  return $s;
}


sub lp_span {
  my $span = shift;
  return '['.join(',',@$span).']';
}

sub lp_shift {
  my $s = shift;
  $s =~ s/^\t/\t\t/msog;
  return $s;
}

sub lp_deriv {
  my $deriv = shift;
  $deriv =~ /d0*(\d+)/;
  return $1;
}

sub Forest::lp_register_dlist {
  my $self = shift;
  my $derivs = shift;
  return $self->{lpdlist}{join(" ",sort map {$_->dids} @$derivs)} ||= ++$self->{lpdlistcount};
}

sub Forest::lp_register_dlist_alt {
  my $self = shift;
  my $derivs = shift;
  return $self->{lpdlist}{join(" ",sort keys %$derivs)} ||= ++$self->{lpdlistcount};
}

sub Forest::lp_register_dlist_list {
  my $self = shift;
  my $derivs = shift;
  return $self->{lpdlist}{join(" ",sort @$derivs)} ||= ++$self->{lpdlistcount};
}

sub Forest::lpdep {
    my $self = shift;
    my $sid = $self->{sid};

    # First pass: collecting dependencies
    $self->collect_dependency;

    my %nodes = ();

    my @clusters = ();
    my @nodes    = ();
    my @edges    = ();
    my @op       = ();
    my @ht       = ();
    my %clusters = ();

    $self->{lpdlistcount} = 0;
    $self->{lpdlist} = {};

    my $lpcluster = sub {
      my ($posa,$posb) = @_;
      my $cid = "${sid}c_".$posa."_".$posb;
      unless (exists $clusters{$cid}) {
	my $cluster = $dependency->{$posa}{$posb};
	my $token = $cluster->{lex} || "";
	$token =~ s/E\d+(\.\d+)?F\d+\|//go;
	my $ltoken = lc($token);
	if ($token ne $ltoken) {
	  ## normalize token to lowercase if there is a node with such a form.
	  foreach my $tok (@{$cluster->{token}}) {
	    $token = $ltoken, last if ($tok eq $ltoken);
	  }
	}
	my $lp =  lp_emit_aux(
			      cluster => {
					  id => lp_quote($cid),
					  left => $posa,
					  right => $posb,
					  lex => lp_quote($cluster->{lex}),
					  token => lp_quote($token)
					 }
			     );
	$clusters{$cid} = lp_quote($cid);
	print "x($lp).\n\n";
      }
      return $clusters{$cid};
    };

    my $lpnode = sub {
      my $node = shift;
      my $id = $node->{nid};
      
      unless (exists $nodes{$id}) {
	my ($posa,$posb)  = @{$node->{pos}};
	$nodes{$id} = {};
	my $cluster = $lpcluster->($posa,$posb);
	my $tree = $node->{tree} || 'void';
	my %features = ( id => lp_quote($id),
			 cluster => $cluster,
			 tree => '['.join(',',map {lp_quote($_)} split(/\s+/,$tree)).']',
			 cat => lp_quote($node->{cat}),
			 lemma => lp_quote($node->{lemma}),
			 lemmaid => lp_quote($node->{lemmaid}),
			 form => lp_quote($node->{token}),
			 deriv => '[]',
			 w => '[]',
			 xcat => "''"
		       );
	if (my $deriv = $node->{deriv}) {
	  $nodes{$id}{derivs} = join(' ',map {$_->dids} @$deriv);
	  $features{deriv} =  $self->lp_register_dlist($deriv);
	}
	exists $trees{$tree} and $features{xcat} = lp_quote($trees{$tree}{cat});
	my $lp = lp_emit_aux(node => \%features);
	$nodes{$id}{lp} = lp_quote($id);
	print "x($lp).\n\n";
      }

      return $nodes{$id}{lp};
      
   };
    
    # Second pass: build lp
    my $edge_handler = 
      sub { my ($self,$target,$edge,$label) = @_;
	    my $source = $edge->{source};
	    $lpnode->($source);
	    ##			     print STDERR "xml dep edge $ida->$idb label=$label type=$target->{type}\n";
	    my %tmp = ();
	    my %edgederivs = ();
	    foreach my $dskel (@{$edge->{deriv}}) {
	      my $dids = $dskel->[0]{prefix};
	      my ($source_op,$target) = @{$dskel->[3]};
	      my $target_op = $target->{id};
	      my $k = "$target_op $source_op";
	      $edgederivs{$dids} = 1;
	      unless (exists $tmp{$k}) {
		my $info = $tmp{$k} = { span => lp_span($target->{span}),
					target_op => "${sid}o$target_op",
					source_op => "${sid}o$source_op",
					names => { $dids => 1}
				      };
		my $reroot = $dskel->[2];
		$reroot and $info->{reroot_source} = $reroot->{nid};
	      } else {
		$tmp{$k}{names}{$dids} = 1;
		if (my $reroot = $dskel->[2]) {
		  if (1 || $tmp{$k}{reroot_source}) {
		    $tmp{$k}{reroot_source} .= " $reroot->{nid}";
		  } else {
		    $tmp{$k}{reroot_source} = $reroot->{nid}; 
		  }
		}
	      }
	    }
	    my @k = keys %tmp;
	    if (scalar(@k) == 1) {
	      ## remove names attr if all derivs from source node go through the edge
	      ## i.e. when the edge is a mandatory one !
	      my $k = $k[0];
	      my $v = $tmp{$k};
	    }
	    my $eid = lp_quote("${sid}".$eid++);
	    my $attrs = {target => $nodes{$target->{nid}}{lp},
			 source => $nodes{$source->{nid}}{lp},
			 id     => $eid,
			 type   => lp_quote($edge->{type}),
			 label  => lp_quote($label),
			 deriv =>  $self->lp_register_dlist_alt(\%edgederivs)
			};
	    lp_emit(edge => $attrs);
	    foreach my $deriv (values %tmp) {
	      my $reroot =  $deriv->{reroot_source} || '[]';
	      my $derivs = $self->lp_register_dlist_alt(\%{$deriv->{names}});
	      lp_emit( edgederiv => {
				     eid => $eid,
				     span => $deriv->{span},
				     source_op => lp_quote($deriv->{source_op}),
				     target_op => lp_quote($deriv->{target_op}),
				     target_node => lp_quote($target->{nid}),
				     reroot => $reroot,
				     derivs => $derivs
				    }
		     );
	    }
	  };

    my $node_handler = 
      sub { my ($self,$target) = @_;
	    $lpnode->($target);
	    $self->edge_loop($target,$edge_handler);
	  };

    my $cluster_handler = 
      sub { my ($self,$posa,$posb,$nodes) = @_;
	    ##				print STDERR "CLUSTER $posa $posb\n";
	    $lpcluster->($posa,$posb);
	    $self->node_loop( $posa, $posb, $nodes, $node_handler );
	  };

    # Third pass: emit

    $self->{sentence} and print <<EOF;
sentence('$self->{sentence}').
EOF

    my $mode = $self->{mode} || 'full';
    print <<EOF;
mode($mode).
EOF

    $self->cluster_loop( $cluster_handler );

    $self->lpdep_op;
    $_->lpdep_ht foreach (values %{$self->{hypertag}});

    ## emit derivation lists
    foreach my $dlist (keys %{$self->{lpdlist}}) {
      my $id = $self->{lpdlist}{$dlist};
##      print "%%processing dlist $id => '$dlist'\n";
      my $notfirst = 0;
      print <<EOF;
dlist($id,[
EOF
      foreach my $did (map {lp_deriv($_)} split(/\s+/,$dlist)) {
	$notfirst and print ",\n";
	print $did;
	$notfirst = 1;
      }
      print <<EOF;
]).

EOF
    }

    return 1;
  }

sub lp_quote {
  my $string = shift || "";
  return $string if ($string =~ /^[a-z_]\w*$/o);
  return $string if ($string =~ /^\d+$/);
  #    $string =~ s/\'/\'\'/og;
  $string =~ s/\'(?!\')/\'\'/og;
  return "\'$string\'";
}

sub Forest::stats {
    my $self = shift;

    # First pass: collecting dependencies
    $self->collect_dependency;

    # Second pass: compute stats
    my $sclusters = 0;
    my $snodes = 0;
    my $sedges = 0;
    my $derivs = 0;

    my $edge_handler = sub { $sedges++; };
    my $node_handler = sub { my ($self,$target) = @_;
			     $snodes++;
			     foreach my $dskel (@{$target->{deriv}}) {
			       $derivs += $dskel->count;
			     }
			     $self->edge_loop( $target, $edge_handler );
			   };
    my $cluster_handler = sub { my ($self,$posa,$posb,$nodes) = @_;
				$sclusters++;
				$self->node_loop( $posa, $posb, $nodes, $node_handler );
			      }; 
    
    $self->cluster_loop( $cluster_handler );
    
##    print "#eskel $eskelcount\n";

    printf "Dependency stats: %2.1f ambiguity %3d clusters %3d nodes %3d edges %d derivs %.1f avgderivs\n",
      (1 + $sedges - $sclusters) / ($sclusters || 1),
	$sclusters,
	  $snodes,
	    $sedges,
	      $derivs,
		$derivs / ($snodes || 1) ;

}

sub Forest::txtdep {
  my $self = shift;
  
  # First pass: collecting dependencies
  $self->collect_dependency;

  my $grammar = $self->{'grammar'};

  # Second pass: emitting

  my %trees = ();
  if (defined $grammar) {
    print "META: Grammar=$grammar\n";
  }
  
  if (0) {
  print <<EOF;
META: <span>/<form>/<lemma>/<cat>/<stree> --{<label>:<kind>:<dir>}--> <span>/<form>/<lemma>/<cat>/<stree>
EOF
}

  my $node_display = sub {
    my $self = shift;
    my $tree = $self->{tree} || 'void';
    my $stree = $tree;
    $stree =~ s/\s.+$//og;
    $trees{$stree} = $tree;
    my $token = $self->{token} || "";
    my $lemma = $self->{lemmaid} || "";
    my $cat = $self->{cat} || "";
    my $span = join(':',@{$self->{pos}});
    return join('/',$span,$token,$lemma,$cat,$stree);
  };
  
  my $edge_handler = 
    sub { my ($self,$target,$edge,$label) = @_;
	  my $source = $edge->{source};
	  my $tinfo = $node_display->($target);
	  my $sinfo = $node_display->($source);
	  my $dir = ($source->{pos}[1] < $target->{pos}[0]) ? 'right' : 'left';
	  print <<EOF;
$sinfo\t--{$label:$edge->{type}:$dir}-->\t$tinfo
EOF
	};

  my $node_handler = 
    sub { my ($self,$target) = @_;
	  $self->edge_loop($target,$edge_handler);
	};
  
  my $cluster_handler =
    sub { my ($self,$posa,$posb,$nodes) = @_;
	  $self->node_loop($posa,$posb,$nodes,$node_handler);
	};

  $self->cluster_loop( $cluster_handler );

  foreach my $stree (keys %trees) {
    print <<EOF;
tree $stree => $trees{$stree}
EOF
  }

}

sub Forest::dependency {
    my $self    = shift;
    my $config = shift;
    my $nb      = $self->{'nb'};
    my $grammar = $self->{'grammar'};
    $forest     = $self;
    my $sid = $self->{sid};

    # First pass: collecting dependencies
    $self->collect_dependency;

    # Second pass: emiting
    if (defined $grammar) {
      print "/* Grammar=$grammar */\n";
    }

    print <<HEADER;
digraph G {
    rankdir = LR;
    ranksep = .5;
    ratio   = auto;
    clusterrank = local;
    charset= "iso-8859-1";
    remincross = 1;
    node [ style = "filled",  fillcolor = "LightGoldenrod1", fontname  = "sans-serif", fontsize = "10.0" ];
    edge [ fontsize = "10.0", fontname  = "sans-serif" ];
HEADER

    my $lex;
    my $cluster;
    my @trees;
    my $start;
    my $secondaries = {};

    my $edge_handler;
    $edge_handler = sub { my ($self, $target, $edge, $label) = @_ ;
			  my $source = $edge->{source};
			  my $end    = node($source);
			  my $type = $edge->{type};
			  my $color  = $color{$type};
			  my $weight = abs($source->{pos}[0]-$target->{pos}[0]);
			  $weight    = 1 unless ($weight);
			     my $deriv = 0;
			     my $w = "";
			     ##			     my $id = "E${sid}".$eid++;
			     my @secondary=();
			     foreach my $dskel (@{$edge->{deriv}}) {
			       $deriv += $dskel->count;
			       defined $dskel->weight and $w = $dskel->weight;
			       if ($type ne 'secondary' && defined $dskel->secondary) {
#				 print "# dependency push secondary edges\n";
				 push(@secondary,@{$dskel->secondary});
			       }
			     }
			     my $width = 1;
			     my $penwidth = "";
			     if ($w) {
			       my $pw = 2 / (1 + exp(-$w/10));
			       $pw < 0.5 and $pw = 0.5;
			       $pw > 4 and $pw = 4.0;
			       $penwidth = sprintf(",penwidth=%.1f ",$pw);
#			       $penwidth = ($w < - 200) ? ",penwidth=0.5" : ($w > 200) ? ",penwidth=2.0" : ",penwidth=1.0";
			       $w = " w=$w";
			     }
			     if ($source->{pos}[0] > $target->{pos}[0]) {
			       print <<EDGE;
     $start -> $end [ label = "$label ($deriv$w)", color = "$color", minlen = $weight, dir = "back" $penwidth];
EDGE
			     } else {
			       print <<EDGE;
     $end -> $start [ label = "$label ($deriv$w)", color = "$color",  minlen = $weight $penwidth];
EDGE
			     }

			  foreach my $secondary (@secondary) {
			    my $op2 = $secondary->[1];
			    my $direct = $secondary->[3];
			    my $source2 = $op2->{dep}{anchors}[0]{dep}{node};
			    my $start2 = node($source2);
#			    print "# emit secondary $secondary->[1] source2=$source2 start2=$start2 target=$target\n";
			    if ($direct) {
			      exists $secondaries->{$start}{$secondary->[2]}{$start2} and next;
			      $secondaries->{$start}{$secondary->[2]}{$start2} = 1;
			      $edge_handler->($self,
					      $target,{ source => $source2,
							deriv => $edge->{deriv},
							type => 'secondary'
						      },
					      $secondary->[2]
					     );
			    } else {
			      exists $secondaries->{$start2}{$secondary->[2]}{$start} and next;
			      $secondaries->{$start2}{$secondary->[2]}{$start} = 1;
			      my $backup = $start;
			      $start = $start2;
			      $edge_handler->($self,
					      $source2,{ source => $target,
							deriv => $edge->{deriv},
							type => 'secondary'
						      },
					      $secondary->[2]
					     );
			      $start = $backup;
			    }
			  }
			     
			};
    my $node_handler = sub { my ($self, $target) = @_;
			     push(@trees, tree_emit($target, $grammar));
			     $start = node($target);
			     $self->edge_loop( $target,  $edge_handler );
			   };
    my $cluster_handler = sub { my ($self,$posa,$posb,$nodes) = @_;
				$cluster = cluster($posa, $posb);
				@trees = ();
				$self->node_loop( $posa, $posb, $nodes, $node_handler );
				## Emit clusters
				my $lex   = $nodes->{'lex'};
				my $trees = join(";\t\n", @trees);
				my $qlex  = $lex;
				if ($config && $config->verbose) {
				  $qlex = "$qlex\\n<$posa:$posb>";
				}
			##	$qlex = "\\$qlex" if ($qlex eq '"');
				$qlex =~ s/"/\\"/og;
				print <<ANCHOR;
subgraph  $cluster {
	    label     = "$qlex";
            style     = "filled";
            fillcolor = "LightYellow1";
	    fontsize  = "10.0";
	    fontname  = "sans-serif";
	    {rank     = same; $trees;}
	}
ANCHOR
			      };

    $self->cluster_loop( $cluster_handler );

    print "}\n\n";
    
  }


sub Forest::tikzdep {
    my $self    = shift;
    my $config = shift;
    my $nb      = $self->{'nb'};
    my $grammar = $self->{'grammar'};
    $forest     = $self;
    my $sid = $self->{sid};

    # First pass: collecting dependencies
    $self->collect_dependency;

    # Second pass: emiting
    if (defined $grammar) {
      print "/* Grammar=$grammar */\n";
    }

    my $lex;
    my $cluster;
    my @trees;
    my $start;

    my @tikzedges = ();

    my $tikzctr=0;
    my $tikzmap = {};
    my $pos = {};

    my $edge_handler = sub { my ($self, $target, $edge, $label) = @_ ;
			     my $source = $edge->{source};
			     my $end    = node($source);
			     my $deriv = 0;
			     my $x = $source->{pos}[0]+1;
			     my $y = $target->{pos}[0]+1;
			     push(@tikzedges,{ source => $start, target => $end, type => $edge->{type}, label => $label });
			   };
    my $node_handler = sub { my ($self, $target,$lex,$tokens,$delta) = @_;
			     push(@trees, tree_emit($target, $grammar));
			     $start = node($target);
			     my $pos_start = $tikzmap->{$start} ||= ++$tikzctr;
			     $pos->{$pos_start} ||= {
						     lemma => $target->{lemmaid} || '_',
						     tree => treename($target->{tree}),
						     cat => $target->{cat} || '_',
						     lex => $lex,
						     incomings => [],
						     tokens => $tokens,
						     delta => $delta
						    };
			     $self->edge_loop( $target,  $edge_handler );
			   };
    my $cluster_handler = sub { my ($self,$posa,$posb,$nodes) = @_;
				$cluster = cluster($posa, $posb);
				@trees = ();
				## Emit clusters
				my $lex   = $nodes->{'lex'};
				$lex ||= '_';
				my $tokens = $lex;
				$lex =~ s/E.*?F\d+\|//og;
				$tokens = join('+',$tokens =~ /E.*?F(\d+)\|\S+/og);
				$tokens ||= '_';
				$self->node_loop( $posa, $posb, $nodes, $node_handler, $lex, $tokens, $posb-$posa);
				my $trees = join(";\t\n", @trees);
			      };

    $self->cluster_loop( $cluster_handler );

    my @sorted =  map {$pos->{$_}} sort {$a <=> $b} keys %$pos;

    my $n = scalar(@sorted);
    my $delta = sprintf("%.3f",1 / (2 * $n));

    print <<HEADER;
\\begin{dependency}
    \\begin{deptext}[column sep=$delta\\textwidth]
HEADER

    print join(' \& ', map {latex_quote($_->{lex})} @sorted),"\\\\\n";
    ($config->_exists('tikzdep_lemma') && $config->get('tikzdep_lemma')) 
      and print join(' \& ', map {latex_quote($_->{lemma})} @sorted),"\\\\ \%\% ligne=lemma \n";
    ($config->_exists('tikzdep_cat') && $config->get('tikzdep_cat')) 
      and print join(' \& ', map {latex_quote($_->{cat})} @sorted),"\\\\ \%\% ligne=cat \n";
    ($config->_exists('tikzdep_trees') && $config->get('tikzdep_trees')) 
      and print join(' \& ', map {latex_quote($_->{tree})} @sorted),"\\\\ \%\% ligne=tree \n";

    print <<EOF;
   \\end{deptext}
EOF

    my %reachables = ();
    foreach my $edge (@tikzedges) {
      my $x = $tikzmap->{$edge->{target}};
      my $y = $tikzmap->{$edge->{source}};
      $edge->{min} = min($x,$y);
      $edge->{max} = max($x,$y);
      $edge->{delta} = $edge->{max} - $edge->{min};
      $reachables{$y} = 1;
    }

    my @lines = ();
    my @below = ();
    foreach my $edge (sort {$a->{delta} <=> $b->{delta}
			      || $a->{min} <=> $b->{min}
				|| $a->{max} <=> $b->{max}}
		      @tikzedges) {
      my $min = $edge->{min};
      my $max = $edge->{max};
      my $line = -1;
      my $crossing = 0;
    LOOP: 
      for(my $l=0; $l <= $#lines; $l++) {
	if ((exists $lines[$l]{$min} && $lines[$l]{$min}{min} < $min)
	    || (exists $lines[$l]{$max} && $lines[$l]{$max}{min} < $max)
	    || (grep {!exists $reachables{$_}} ($min+1)..($max-1)) # cross with root node
	   ) {
	  ## potential crossing
	  $crossing = 1;
	  last LOOP;
	}
	## search for a line
	for (my $i=$min; $i < $max; $i++) {
	  next LOOP if (exists $lines[$l]{$i});
	};
	## Found a potential line
	$line=$l;
	last;
      }
      if (!$crossing) {
	if ($line == -1 ) {
	  push(@lines,{});
	  $line = $#lines;
	}
	# toggle (l,i) cases to state they are busy
	for (my $i=$min; $i < $max; $i++) {
	  $lines[$line]{$i}=$edge;
	};
	$edge->{line} = $line;
      } else {
	# crossing
      LOOP2:
	for (my $l=0; $l <= $#below; $l++) {
	  ## search for a line below
	  for (my $i=$min; $i < $max; $i++) {
	    next LOOP2 if (exists $below[$l]{$i});
	  };
	  ## Found a potential line
	  $line=$l;
	  last;
	}
	if ($line == -1 ) {
	  push(@below,{});
	  $line = $#below;
	}
	# toggle (l,i) cases to state they are busy
	for (my $i=$min; $i < $max; $i++) {
	  $below[$line]{$i}=$edge;
	};
	$edge->{line} = -(1+$line);
      }
    }

    foreach my $edge (@tikzedges) {
      my $x = $tikzmap->{$edge->{target}};
      my $y = $tikzmap->{$edge->{source}};
      my $label = latex_quote($edge->{label});
      my $delta = abs($x-$y);
      my $unit = 1;
#      $delta > 4 and $unit = 0.7;
#      $delta > 7 and $unit = 0.5;
      if ($edge->{line} ge 0) {
	$unit = sprintf("%.3f",10 * (1+$edge->{line}) / ($delta+0.1));
	print <<EOF;
\\depedge[$edge->{type}, edge unit distance=${unit}]{$x}{$y}{$label}
EOF
      } else {
	$unit = sprintf("%.3f",- 10 * $edge->{line} / ($delta+0.1));
	print <<EOF;
\\depedge[edge below,$edge->{type}, edge unit distance=${unit}]{$x}{$y}{$label}
EOF
      }

    }
    print <<EOF;
\\end{dependency}
EOF
    
  }

sub latex_quote {
  my $s = shift;
  $s =~ s/\\([?+*])/$1/og;
  $s =~ s/_/\\_/og;
  return $s;
}

sub is_lexical_type {
  my $type = shift;
  return $type eq 'anchor' || $type eq 'lexical' || $type eq 'epsilon';
}



sub Forest::depconll {
    my $self    = shift;
    my $config = shift;
    my $nb      = $self->{'nb'};
    my $grammar = $self->{'grammar'};
    $forest     = $self;
    my $sid = $self->{sid};

    # First pass: collecting dependencies
    $self->collect_dependency;

    # Second pass: emiting
    my $lex;
    my $cluster;
    my @trees;
    my $start;

    my $posctr=0;
    my $posmap = {};
    my $pos = {};
    my @edges = ();

    # if (1) {
    #   my @ht = keys %{$self->{hypertag}};
    #   print "ALL HT @ht\n";
    #   foreach my $htid (@ht) {
    # 	my $ht = $self->{hypertag}{$htid};
    # 	my $derivs = $ht->{derivs};
    # 	print "\tHT $htid => @{$derivs}\n";
    #   }
    # }

    # my $hypertags = {};
    # foreach my $head (keys %{$self->{op}}) {
    #   my $op = $self->{op}{$head};
    #   foreach my $deriv (@{$op->{derivations}}) {
    # 	my $dids = $deriv->{dids}->dids;
    # 	print "HERE $deriv $dids <$deriv->{ht}>\n";
    # 	if (defined $deriv->{ht}) {
    # 	  print "DEBUGHT map $dids => $deriv->{ht}\n";
    # 	  $hypertags->{$dids} = $deriv->{ht};
    # 	}
    #   }
    # }

    my $edge_handler = sub { my ($self, $target, $edge, $label) = @_ ;
			     my $source = $edge->{source};
			     my $end    = node($source);
			     my $deriv = 0;
			     my $x = $source->{pos}[0]+1;
			     my $y = $target->{pos}[0]+1;
			     push(@edges,{ source => $start, target => $end, type => $edge->{type}, label => $label });
			   };
    my $node_handler = sub { my ($self, $target,$lex,$tokens,$delta) = @_;
			     push(@trees, tree_emit($target, $grammar));
			     $start = node($target);
			     my $pos_start = $posmap->{$start} ||= ++$posctr;
			     $pos->{$pos_start} ||= {
						     lemma => $target->{lemmaid} || '_',
						     tree => treename($target->{tree}),
						     cat => $target->{cat} || '_',
						     lex => $lex,
						     incomings => [],
						     tokens => $tokens,
						     delta => $delta,
						     deriv => $target->{deriv} || undef
						    };
			     # print "DEBUG $pos_start target=$target\n";
			     # foreach my $key (keys %$target) {
			     #   print "\t$key: $target->{$key}\n";
			     # }
			     # print "\tderivs: @{$target->{deriv}}\n";
			     # foreach my $d (@{$target->{deriv}}) {
			     #   print "\t",$d->dids,"\n";
			     # }
			     $self->edge_loop( $target,  $edge_handler );
			   };
    my $cluster_handler = sub { my ($self,$posa,$posb,$nodes) = @_;
				$cluster = cluster($posa, $posb);
				@trees = ();
				my $lex  = $nodes->{'lex'};
				$lex ||= '_';
				my $tokens = $lex;
				$lex =~ s/E.*?F\d+\|//og;
				$tokens = join('+',$tokens =~ /E.*?F(\d+)\|\S+/og);
				$tokens ||= '_';
				$self->node_loop( $posa, $posb, $nodes, $node_handler,$lex,$tokens,$posb-$posa);
				## Emit clusters
				my $trees = join(";\t\n", @trees);
			      };

    $self->cluster_loop( $cluster_handler );

    foreach my $edge (@edges) {
      my $psource = $edge->{psource} = $posmap->{$edge->{source}};
      my $ptarget = $edge->{ptarget} = $posmap->{$edge->{target}};
      push(@{$pos->{$psource}{incomings}},$edge);
    }

    my %tokens = ();

    foreach my $x (sort {$a <=> $b} keys %$pos) {
      my $entry = $pos->{$x};
      @{$entry->{incomings}} or push(@{$entry->{incomings}},{ ptarget => 0,  label => 'root' });
      my $heads = join('|',map {$_->{ptarget}} @{$entry->{incomings}});
      my $labels = join('|',map {$_->{label}} @{$entry->{incomings}});
      my $tokens = $entry->{tokens};
      my $lex = $entry->{lex};
      #  we don't remit a form if already emitted
      (exists $tokens{$tokens}) and $lex = "_";
      $tokens{$tokens} = 1;
      ## for reused tokens (copy-like) in coordinations, we prefix with *
      ($entry->{delta} == 0 && $tokens ne '_') and $tokens = '*'.$tokens;
      my $ht = '_';
      my $derivs = $entry->{deriv};
      if (defined $derivs && @$derivs) {
	my $htid = $derivs->[0]{ht};
	if ($htid) {
#	  print "DEBUG htid=$htid\n";
	  $ht = $self->{hypertag}{$htid}->depconll();
	}
      }
      print join("\t",
		 $x,		# id
		 $lex,	# lex
		 $entry->{lemma}, # lemma
		 $entry->{cat},	  # cat
		 '_',		  # flat feature structure (to be filled)
		 $heads,	  # list of head ids
		 $labels,	  # list of dependency labels
		 '_',		  # (list of non projectives heads)
		 '_',		  # (list of non projective labels)
		 $tokens, # new: lists of token id
		 $ht	# new: hypertag info
		),"\n";
    }
    print "\n";

  }

sub Forest::Hypertag::depconll {
  my $self = shift;
  my @res = ();
  my $ht = $self->{ht};
  push(@res,
       map {$ht->{f}{$_}->depconll()}
       grep {exists $ht->{f}{$_}}
       qw{refl imp diathesis extraction distrib ctrsubj}
      );
  return join('|',@res);
}

sub Forest::Val::depconll {
  return $_[0]->{val};
}

sub Forest::Minus::depconll {
  return '-';
}

sub Forest::Plus::depconll {
  return '+';
}

sub Forest::Comp::depconll {
  my $self = shift;
  my $name = $self->{comp};
  return map {"$name=$_"} map {$_->depconll} @{$self->{value}};
}



sub Forest::Op::dependency {
    ## returns a list of anchors
    ## and attach a dependency node for anchor ops
    my $self = shift;
    my $tree = shift;
    unless (exists $self->{dep}) {
      my $type = $self->{type};
##      print "process $self->{id} $self->{cat} $self->{lemma} $type\n";
      if (is_lexical_type($type)) {
	return unless (defined $tree);
	my $cat   = $self->{cat} || "";
	my $lemma = $self->{lemma} || "";
	my $lemmaid = $self->{lemmaid} || $lemma;
	my ($left,$right) = @{$self->{span}};
	my $entry =  $dependency->{$left}{$right} ||= {};
	$entry->{lex} ||= $self->{lex};
	## Deprecated: to be removed in some future
	## replaced par 'token/form' on node (rather than token on cluster)
	push(@{$entry->{token}},$self->{token});
	my $xtree = ($type eq 'epsilon') ? "" : ($type eq 'lexical')  ? "lexical" : $tree;
	## following $e corresponds to a node for $cat $lemma and $xtree
	my $e = $entry->{tree} ||= {};
	$e = $e->{$cat} ||= {};
	$e = $e->{$lemmaid} ||={};
	$e = $e->{$xtree} 
	  ||= { nid => "$forest->{sid}".$nid++,
		cat => $cat,
		lemma => $lemma,
		lemmaid => $lemmaid,
		tree => $xtree,
		token => $self->{token},
		pos => [$left,$right]
	      };
	$self->{dep} = { anchors => [$self], 
			 node => $e
		       };
      } else {
	# we return the distinct anchors of the various derivations for op
	## a same anchor may be used for several derivations of a same op
	## => we remove duplicates
	my %anchors = ();
##	print "#derivation op=$self->{id} ".scalar(@{$self->{'derivations'}})."\n";
	if (exists $self->{'derivations'}) {
	  $anchors{$_} = $_ foreach (map {(defined $_) and $_->dependency($self)} @{$self->{'derivations'}});
	}
	$self->{dep} = { anchors => [values %anchors ] };
      }
##      print "\t$self->{id} anchors=".join(' ',map {$_->{id}} @{$self->{dep}{anchors}})."\n";
    }
    return $self->{dep}{anchors};
  }

sub Forest::Deriv::dependency_aux {
    my $self  = shift;
    my $tree = shift;
    my $daux = {};
    my $anchors = $daux->{anchors} = [];
    my $edges = $daux->{edges} = [];
    my $dskel = $daux->{dskel} = [];
    my $lexicals = $daux->{lexicals} = {};
    foreach my $label (keys %{$self->{nodes}}) {
      foreach my $target (grep {defined $_} map {$forest->{op}{$_}} @{$self->{nodes}{$label}}) {
##	print "AUX EDGE $label $target->{type}\n";
	if ($target->{type} eq 'anchor'
	   ##  && $target->{'span'}[0] < $target->{'span'}[1] 
	   ){
	  push(@$anchors,$target);
	  $trees{$tree}{node} = $label;
	  next;
	}
	($target->{type} eq 'lexical') and $lexicals->{$label} = $target;
	if ($label =~ /^\d+/) {
	  my $cat =  $target->{'cat'};
	  $label = "$cat\#$label";
	}
	my $tanchors = $target->dependency($tree);
	my $edge = { type       => $target->{type},
		     label      => $label,
		     anchors    => $tanchors,
		     targetspan => $target->{span}, # this one should be removed in favor of target
		     target     => $target
		   };
	if ( exists $self->{reroot}{$label}{$target->{id}}
	   ) {
	  $edge->{reroot} = $self->{reroot}{$label}{$target->{id}};
	}
	if ( exists $self->{weights}{$label}{$target->{id}} ){
	  $edge->{weight} = $self->{weights}{$label}{$target->{id}};
	}
	if (exists $self->{secondary}{$target->{id}}) {
#	  print "found secondary on $label $target->{id} : @{$self->{secondary}{$target->{id}}}\n";
	  $edge->{secondary} = $self->{secondary}{$target->{id}};
	}
	push(@$edges, $edge );
	if ($tanchors && scalar(@$tanchors) > 1) {
	  my $i=1;
	  push(@$dskel,{ map {$_ => $i++} @$tanchors });
	}
      }
    }
    return $daux;
  }

sub Forest::Deriv::dependency {
    # return an anchor
    my $self  = shift;
    my $xop    = shift;
    my $opid = $xop->{id};
    my $tree  = $self->{tree} || 'lexical';
    my $ht = $self->{ht};
    my $did   = $self->{id} ||= "$forest->{sid}".$dcounter++;
    $trees{$tree} = { cat   => $xop->{cat} , 
		      lemma => $xop->{lemma} || ""
		    };
##    print "process deriv $did <$ht> $tree\n";

##    print "DID $did $tree $opid\n";
    my $daux = $self->dependency_aux($tree);

    my $lexicals = $daux->{lexicals};
    my $edges = $daux->{edges};
    my $anchors = $daux->{anchors} ||= [];
    
    ## WARNING: we may have cases of derivations with no anchor and no indication of tree
    ## what should we do ? maybe use the anchor of some node in deriv as anchor
    ##    print "Deriv for $opid $xop->{cat} @{$xop->{span}} $self->{tree}: anchor=@$anchors edges=@$edges\n";

    unless (@$anchors) {
      ## We create a pseudo anchor,
      my $xid = "$tree @{$xop->{span}}";
      my $dummy = $forest->{pseudoop}{$xid}
	||= Forest::Op->new( id    => $xid,
			     type  => 'anchor',
			     lex   => "",
			     token => "",
			     lemma => "",
			     lemmaid => "",
			     deriv => {},
			     tree  => $tree,
			     span  => [$xop->{span}[0],$xop->{span}[0]],
			     cat   => $xop->{cat} 
			   );
      $dummy->dependency($tree);
      push(@$anchors,$dummy);
    }
    my $anchor = $anchors->[0];
    $anchor->dependency($tree);
    ## $sanchor correspond to the start node
    my $sanchor = $anchor->{dep}{node};
    
    # Skeletons of all possible derivation indexes
    # when ambiguity for the head of the target of a dependency, we have to duplicate the derivation id
    my $dskel = $daux->{dskel};
    
    ##    my @dids = map("$did$_",gen_did(0,$dskel));
    my $AllDerivs = DerivSkel->new({ prefix => "$did",
				     filter => 0,
				     dskel => $dskel,
				     ht => $self->{ht}
				   });
    push(@{$sanchor->{deriv}}, $AllDerivs);
    $self->{dids} = $AllDerivs;
    
    foreach my $edge (@$edges) {
      ## for each edge, we store a dep link $info
      ## from each target anchors
      ## to the source anchor (modulo some rerouting for adj on lexical coanchors)
      ## To each dep link, we build and maintain
      ## the list of derivs going through this link
      my $label = $edge->{label};
      my $type = $edge->{type};
##      print "EDGE $label $type\n";
      my $amb = scalar(@{$edge->{anchors}}) > 1;
      foreach my $start (@{$edge->{anchors}}) {
	my $reroot = 0;
	## To handle operations on lexical nodes that should be attached to them
	## rather than to the anchor of the tree
	my $source = $anchor;
	my $stree  = $start->{tree} || "";
	## the tree of a lexical is the same tree of the anchor
	($type eq 'lexical') and $stree = 'lexical';
	if (exists $edge->{reroot}) {
	  $source = $forest->{op}{$edge->{reroot}};
	  $reroot = $anchor->{dep}{node};
	  $source->dependency($source->{tree});
	  ## print "Reroot source $reroot\n";
	} elsif ($type eq 'adj' && exists $lexicals->{$label}) {
	  $source = $lexicals->{$label};
	  $reroot = $anchor->{dep}{node};
	  ## WARNING: This rerooting of adj on lexical nodes is not yet complete
	} 
	my $sdep = $start->{dep}{node};
	my $sedges = $sdep->{label}{$label} ||= [];
	my $xedges = $sdep->{xlabel}{$label} ||= {};

	## inlined creation of EdgeDerivSkel
	my $eskel = bless [ $AllDerivs, # 0
			    $start,	# 1
			    $reroot,	# 2
			    [$opid,$edge->{target},$edge->{weight}],	# 3
			    undef,		# count 4
			  ], 'EdgeDerivSkel';

	$amb or $eskel->[4] = $AllDerivs->{memocount};

	if (exists $edge->{secondary}) {
	  ##	  print "pushing secondary on eskel\n";
	  foreach my $secondary (@{$edge->{secondary}}) {
	    my $source2 = $forest->{op}{$secondary->[1]};
#	    $source2->dependency($source2->{tree});
#	    print "pushing secondary on eskel $secondary->[0] $secondary->[1]:$source2 $secondary->[2]\n";
	    push(@{$eskel->[5]},[$secondary->[0],$source2,$secondary->[2],$secondary->[3]]);
	  }
	}

	##	$eskelcount++;
	##	$amb or $eskel->set_like($AllDerivs);
	my $xsource = $source->{dep}{node} ||= {};
	if (my $x=$xedges->{$type}{$xsource}) {
	  push(@{$x->{deriv}},$eskel);
	} else {
	  push(@$sedges,
	       $xedges->{$type}{$xsource} = { type  => $type,
					      source => $xsource,
					      deriv => [ $eskel ]
					    });
	}
      }
    }
    return $anchor;
  }

sub norm_tree {
    my $tree = shift || "";
#    print "/* norm tree $tree */\n";
    $tree =~ tr/-/_/;
    $tree =~ s/\s+/_/og;
    return $tree;
}

sub Forest::xmldep_ht {
  my $self = shift;
  my $GEN  = shift;

  foreach my $head (keys %{$self->{'op'}}) {
    $self->{'op'}{$head}->xmldep_ht($GEN);
  }

}

sub Forest::Hypertag::xmldep_ht {
  my $self  = shift;
  my $GEN   = shift;
  my $ht = $self->{ht};
  my $derivs = xml_derivs(\@{$self->{xderivs}});
  ## derivs set by xmldep_op
  ## reinitialize after xmldep_ht
  $self->{derivs}=[];		
  $GEN->hypertag({ derivs => $derivs,
		   id => $self->{id}
		 },
		 $ht ? $ht->pretty_print(2) : ()
		)->emit("  ");
}

sub Forest::Op::xmldep_ht {
  my $self  = shift;
  my $GEN   = shift;
  my $type  = $self->{type};
  my $id    = $self->{'id'};
  return if ($type eq 'anchor' || $type eq 'lexical');
 
  foreach (@{ $self->{'derivations'}}) {
    $_->xmldep_ht($GEN,$id);
  }
}

sub Forest::Deriv::xmldep_ht {
  my $self = shift;
  my $GEN   = shift;
  my $opid = shift;
  my $ht = $self->{ht};
  if (defined $ht) {
    $GEN->hypertag({ derivs => $self->{dids}->dids,
		     op => $opid,
		     id => "$forest->{sid}".$htid++
		   },
		   $ht->pretty_print(2)
		  )->emit("  ");
  }
}


sub Forest::xmldep_op {
  my $self = shift;
  my $GEN  = shift;

  foreach my $head (keys %{$self->{'op'}}) {
    $self->{'op'}{$head}->xmldep_op($GEN);
  }

}

sub Forest::Op::xmldep_op {
  my $self  = shift;
  my $GEN   = shift;
  my $id    = $self->{'id'};
  my $type  = $self->{type};
  return if ($type eq 'anchor' || $type eq 'lexical');
  my @deriv = ();
  my $derivs = xml_derivs([map {$_->xmldep_op($GEN,$id)} @{$self->{derivations}}]);

  my @content=();
  push(@content,$self->{'top'}->pretty_print(2,'top')) if (defined $self->{'top'});
  push(@content,$self->{'bot'}->pretty_print(2,'bot')) if (defined $self->{'bot'});
  $GEN->op({id => "$forest->{sid}o$id", 
	    span => join(' ', @{$self->{span} || []}), 
	    deriv => $derivs, 
	    cat => $self->{cat}}, 
	   @content)->emit("  ");
}

sub Forest::Deriv::xmldep_op {
  my $self = shift;
  my $dids = $self->{dids}->dids;
  if (defined $self->{ht}) {
    my $ht = $forest->{hypertag}{$self->{ht}}
      ||= Forest::Hypertag->new( id => $self->{ht},
				 xderivs => [] );
    push(@{$ht->{xderivs}},$dids);
  }
  return $dids;
}

sub Forest::lpdep_op {
  my $self = shift;

  foreach my $head (keys %{$self->{'op'}}) {
    $self->{'op'}{$head}->lpdep_op;
  }

}

sub Forest::Op::lpdep_op {
  my $self  = shift;
  my $id    = $self->{'id'};
  my $type  = $self->{type};
  return if ($type eq 'anchor' || $type eq 'lexical');
  my @deriv = map {$_->xmldep_op($id)}  @{$self->{derivations}};
  my $derivs = $forest->lp_register_dlist_list(\@deriv);
  
  my %features = ( id => lp_quote("$forest->{sid}o$id"),
		   span => lp_span($self->{span} || []),
		   cat => lp_quote($self->{cat}),
		   deriv => $derivs
		 );

  defined $self->{top} and $features{top} = $self->{top}->LP;
  defined $self->{bot} and $features{bot} = $self->{bot}->LP;

  lp_emit(op => \%features);

}

sub Forest::Hypertag::lpdep_ht {
  my $self  = shift;
  my $opid = shift;
  my $ht = $self->{ht};
  my $derivs = $forest->lp_register_dlist_list($self->{derivs});
  ## derivs set by xmldep_op
  ## reinitialize after xmldep_ht
  $self->{derivs}=[];
  my %features = ( id => lp_quote($self->{id}),
		   deriv => $derivs,
		   ht => $ht ? $ht->LP : "[]",
##		   op => $opid
		 );
  lp_emit(hypertag => \%features);
}

sub Forest::Feature::LP {
    my $self = shift;
    my $x = lp_emit_list(map $self->{'f'}{$_}->LP, sort (keys %{$self->{'f'}}));
    return "fs($x)";
}

sub Forest::Comp::LP {
  my $self = shift;
  my $name = $self->{comp};
  my $var = $self->{id};
  my $v = '['.join(',',map $_->LP, @{$self->{value}}).']';
  $var and $v = "var($var,$v)";
  return "$name: $v";
}

sub Forest::Val::LP {
    my $self = shift;
    return lp_quote($self->{val});
}

sub Forest::Plus::LP {
    my $self = shift;
    return "(+)" ;
}

sub Forest::Minus::LP {
    my $self = shift;
    return "(-)" ;
}



package DerivSkel;
#our @ISA = qw/DerivSkel/;

sub new {
  my ($this,$self) = @_;
  my $class  = ref($this) || $this;

  my $filter = $self->{filter} || 0;
  my $n = 1;
  $n *= scalar(keys %$_) foreach (grep {!exists $_->{$filter}} @{$self->{dskel}});
  $self->{memocount} = $n;

  bless $self, $class;
}

sub dids { $_[0]->{prefix}}
sub info { $_[0]{info}}
sub reroot { $_[0]{reroot} }
sub count { $_[0]->{memocount} }

package EdgeDerivSkel;
use List::Util qw{reduce min max};
#our @ISA = qw/DerivSkel/;

sub new {
  my ($this,%info) = @_;
  my $class  = ref($this) || $this;
  my $self = [ $info{parent}, # 0
	       $info{filter},	# 1
	       $info{reroot},	# 2
	       $info{info},	# 3
	       undef,		# count 4
##	       $info->{amb}	# 5
	     ];
#  $info{amb} or $self->[4] = $self->[0]->count;
  $info{amb} or $self->[4] = $self->[0]{memocount};
  bless $self, $class;
}

sub dids { $_[0][0]{prefix};}
sub info { $_[0][3];}
sub reroot { $_[0][2]; }

sub weight { $_[0][3][2]; }

sub count {
  my $self = shift;
  return $self->[4] ||= do {
    my $filter = $self->[1];
    my $n = 1;
    $n *= scalar(keys %$_) foreach (grep {!exists $_->{$filter}} @{$self->[0]{dskel}});
    $n;
  }
}

sub secondary {
  my $self = shift;
  if (scalar(@$self) == 6) {
    return $self->[5];
  } else {
    return undef;
  }
}

package MyGenerator;

## A very specific XML generator to be much faster than using XML::Generator

sub new {
  my $class = shift;
  return {}, $class;
}

sub node {
  my $self = shift;
  my $a = shift;
  return MyGenerator::Elt->new("node",$a);
}

sub edge {
  my $self = shift;
  my $a = shift;
  return MyGenerator::Elt->new("edge",$a,@_);
}

sub deriv {
  my $self = shift;
  my $a = shift;
  return simple("deriv",$a);
}

sub op {
  my $self = shift;
  my $a = shift;
  return MyGenerator::Elt->new("op",$a,@_);
}

sub cluster {
  my $self = shift;
  my $a =shift;
  return MyGenerator::Elt->new("cluster",$a);
}

sub dependencies {
  my $self = shift;
  my $a={};
  $a = shift if (ref($_[0]) eq "HASH");
  return MyGenerator::Elt->new("dependencies",$a,@_);
}

sub narg {
  my $self = shift;
  my $a=shift;
  return MyGenerator::Elt->new("narg",$a,@_);
}

sub hypertag {
  my $self = shift;
  my $a = shift;
  return MyGenerator::Elt->new("hypertag",$a,@_);
}

sub fs {
  my $self = shift;
  my $a={};
  $a = shift if (ref($_[0]) eq "HASH");
  return MyGenerator::Elt->new("fs",$a,@_);
}

sub f {
  my $self = shift;
  my $a={};
  $a = shift if (ref($_[0]) eq "HASH");
  return MyGenerator::Elt->new("f",$a,@_);
}

sub minus {
  return "<minus/>";
}

sub plus {
  return "<plus/>"
}

sub val {
  my $self = shift;
  my $val = shift;
  return "<val>$val</val>";
}

sub simple {
  my ($name,$attrs) = @_;
  my $a = '';
  foreach my $k (keys %$attrs) {
    my $v = $attrs->{$k};
    next unless (defined $v);
    $a .= " $k=\"$v\"";
  }
  return "<$name$a/>";
}

package MyGenerator::Elt;

sub new {
  my ($class,$name,$attrs,@content) = @_;
  return bless { name => $name, 
		 attrs => $attrs,
		 content => [@content]
	       }, $class;
}

sub emit {
  my $self = shift;
  my $pref = shift;
  my $txt = "$pref<$self->{name}";
  foreach my $k (keys %{$self->{attrs}}) {
    my $v = $self->{attrs}{$k};
    next unless (defined $v);
    $txt .= " $k=\"$v\"";
  }
  if (@{$self->{content}}) {
    $txt .= ">\n";
    print $txt;
    my $spref = "$pref ";
    foreach (@{$self->{content}}) {
      if (ref($_)) {
	$_->emit($spref);
      } else {
	print "$spref$_\n";
      }
    }
    print "$pref","</$self->{name}>\n";
  } else {
    $txt .= "/>\n";
    print $txt;
  }
  delete $self->{content};
  delete $self->{attrs};
}

1;

__END__

=head1 NAME

Forest::Dependency::Writer

=head1 DESCRIPTION

Writes forest of dependencies in dot format to build a graph.

=head1 SYNOPSIS

  use Forest::Dependency::Writer;

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

