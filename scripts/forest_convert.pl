#!/usr/bin/perl

require 5.005;
use strict;
use warnings;
use Forest;
use Forest::LP::Parser;
use Forest::RCG::Parser;
use Forest::XTAG::Parser;
use Forest::XML::Parser;
use Forest::Dependency::Reader;
##use Forest::XML::Writer;
use Forest::HTML::Writer;
use Forest::Dot::Writer;
use Forest::Dependency::Writer;
use Forest::Tagger::Writer;
use Data::Dumper;
use AppConfig qw/:argcount :expand/;

my %reader =
  ('lp'      => \&Forest::LP::Parser::parse,
   'rcg'     => \&Forest::RCG::Parser::parse,
   'xtag'    => \&Forest::XTAG::Parser::parse,
   'xml'     => \&Forest::XML::Parser::parse,
   'xmldep'  => \&Forest::Dependency::Reader::parse
  );

my %writer = 
  ('xml'     => \&xml_dump,
   'dep'     => \&dependency_dump,
   'tikzdep'     => \&tikzdependency_dump,
   'xmldep'  => \&xmldep_dump,
   'depxml'  => \&xmldep_dump,	# for dislexics
   'lpdep'   => \&lpdep_dump,
   'txtdep'   => \&txtdep_dump,
   'deptxt'   => \&txtdep_dump,
   'stats'   => \&stats_dump,
   'tree'    => \&tree_dump,
   'grammar' => \&html_dump,
   'tagger'  => \&tagger_dump,
   'yesno'   => \&yesno_dump,
   'line'    => \&line_dump,
   'dump'    => \&dump,
   'info'    => \&info_dump,
   'depconll' => \&depconll_dump,
   'cfg' => \&cfg_dump,
  );

my $iformat = 'lp';
my $oformat = 'xml';

my $config = AppConfig->new(
			    { CREATE => 1 },
			    "in|i=s"    => {DEFAULT => ''},
			    "out|o=s"   => {DEFAULT => ''},
			    "from|f=s"  => {DEFAULT => ''},
			    "to|t=s"    => {DEFAULT => ''},
			    "keep=i"  => {DEFAULT => 10},
			    "maxlist=i" => {DEFAULT => 700},
			    "verbose|v!" => { DEFAULT => 0 }
                           );
$config->args();

$iformat = $config->from unless (!$config->from);
$oformat = $config->to   unless (!$config->to);

my $forest;

if (@ARGV) {
    my $input = shift @ARGV;
    open(INPUT, $input) or die "unable to open '$input': $!";
    $forest=$reader{$iformat}->(\*INPUT,{ keep => $config->keep,
					  maxlist => $config->maxlist,
					  verbose => $config->verbose
					});
    close(INPUT);
}
else {
    $forest=$reader{$iformat}->(\*STDIN,{ keep => $config->keep,
					  maxlist => $config->maxlist,
					  verbose => $config->verbose
					}); # pipe
}

$writer{$oformat}->($forest,$config);    

sub dump {
  my $forest = shift;
  my $d = Data::Dumper->new([$forest]);   
  print $d->Dump,"\n\n";
}

sub xml_dump {
  my $forest = shift;
  $forest->pretty_print(@_);
}

sub html_dump {
    $forest->html(@_);
}

sub tree_dump {
  my $forest = shift;
  $forest->dot(@_);
}

sub dependency_dump {
    my $forest = shift;
    $forest->dependency(@_);
}

sub tikzdependency_dump {
    my $forest = shift;
    $forest->tikzdep(@_);
}

sub xmldep_dump {
  my $forest = shift;
  $forest->xmldep(@_);
}

sub lpdep_dump {
  my $forest = shift;
  $forest->lpdep(@_);
}

sub txtdep_dump {
  my $forest = shift;
  $forest->txtdep(@_);
}

sub depconll_dump {
  my $forest = shift;
  $forest->depconll(@_);
}

sub tagger_dump {
  my $forest = shift;
  $forest->tagger(@_);
}

sub stats_dump {
    my $forest = shift;
    $forest->stats(@_);
}

sub yesno_dump {
  my $forest = shift;
  my $status = "Failure";
  if ($forest->{nb} >= 0) {
    $status = "Success";
    $status = "Partial" if (exists $forest->{mode} && $forest->{mode} eq 'robust');
  }
  print "$status\n";
}

sub line_dump {
  use Forest::Line::Writer;
  my $forest = shift;
  $forest->line(@_);
}

sub info_dump {
  use Forest::Info::Writer;
  my $forest = shift;
  $forest->info(@_);
}

sub cfg_dump {
  use Forest::CFG::Writer;
  my $forest = shift;
  $forest->CFG(@_);
}

=head1 NAME

forest_convert

=head1 SYNOPSIS

./forest_convert.pl [options] 	 
  	 
where the options are 	 
 	 
=over 4 	 
  	 
=item -i <in>

=item -o <out>

=item -f <from>       where <from> can be: lp|rcg|xtag|xml|xmldep

=item -t <to>         where <to> can be: xml|dep|xmldep|lpdep|stats|tree|grammar|tagger|yesno|line|dump|info

=item -keep <limit>   keeps derivations with at most <limit> adjunctions per node (when ambiguities)

=item -maxlist <limit>   keeps at most <limit> derivations per node derivation list (when ambiguities)

=back	 

To display the output as a gif file, use option -t dep and redirect it:

./forest_convert.pl -i <forest> -f <from option> -t dep | dot -Tgif | display

=head1 DESCRIPTION

B<forest_convert.pl> converts between various
formats for shared derivation forest produced by parsers for Tree
Adjoining Grammars [TAG].

B<forest_convert.pl> is also a component of ALPAGE Linguistic Processing Chain for French. 
Please check L<http://alpage.inria.fr/installatoll.en.html>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

=over 4

=item * B<forest_utils> on ALPAGE website L<http://alpage.inria.fr/catalogue.en.html#forest_utils>

=item * Inria GForge project page L<http://lingwb.gforge.inria.fr>

=back

=cut
