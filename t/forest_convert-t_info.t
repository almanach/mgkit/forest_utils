#!/usr/bin/perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => scalar @data;

# example: 'La Commission voit que les prix montent.'
my $dir = dirname($0);
my @output = split /\n/, `scripts/forest_convert.pl -f lp -t info $dir/forest`;

foreach my $expected_output (@data) {
    chomp $expected_output;
    is(shift @output, $expected_output, "$expected_output");
}


__DATA__
1: 2 derivations
2: 2 derivations
3: 1 derivations
4: 1 derivations
5: 1 derivations
7: 1 derivations
8: 1 derivations
10: 1 derivations
11: 1 derivations
12: 1 derivations
13: 1 derivations
15: 1 derivations
17: 1 derivations
19: 1 derivations
20: 1 derivations
21: 1 derivations
