#!/usr/bin/perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => scalar @data;

# example: 'La Commission voit que les prix montent.'
my $dir = dirname($0);
my @output = split /\n/, `scripts/forest_convert.pl -f lp -t stats $dir/forest`;

foreach my $expected_output (@data) {
    chomp $expected_output;
    is(shift @output, $expected_output, "$expected_output");
}


__DATA__
Dependency stats: 0.0 ambiguity   9 clusters   9 nodes   8 edges 9 derivs 1.0 avgderivs
