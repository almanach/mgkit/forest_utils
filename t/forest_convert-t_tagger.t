#!/usr/bin/perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => scalar @data;

# example: 'La Commission voit que les prix montent.'
my $dir = dirname($0);
my @output = split /\n/, `scripts/forest_convert.pl -f lp -t tagger $dir/forest`;

foreach my $expected_output (@data) {
    chomp $expected_output;
    is(shift @output, $expected_output, "$expected_output");
}


__DATA__
0	1	det	La	[def : +, det : +, gender : fem, number : sg, wh : -]
1	2	nc	Commission	[def : +, gender : fem, hum : -, number : sg, person : 3, time : -]
2	3	v	voit	[lightverb : -, mode : indicative, number : sg, person : 3, tense : present]
3	4	pri	que	[case : acc]
4	5	det	les	[def : +, det : +, number : pl, wh : -]
5	6	nc	prix	[def : +, gender : masc, hum : -, person : 3, time : -]
6	7	v	montent	[lightverb : -, mode : indicative|subjonctive, number : pl, person : 3, tense : present]
7	8	_	.	[]
