#!/usr/bin/perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => scalar @data;

# example: 'La Commission voit que les prix montent.'
my $dir = dirname($0);
my @output = split /\n/, `scripts/forest_convert.pl -f lp -t yesno $dir/forest`;

foreach my $expected_output (@data) {
    chomp $expected_output;
    is(shift @output, $expected_output, "$expected_output");
}


__DATA__
Success
