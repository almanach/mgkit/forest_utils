#!/usr/bin/perl -w
# $Id$

use strict;
use Test::More;

eval { 
    require Test::Kwalitee; 
    Test::Kwalitee->import( tests =>
			    [ qw( -has_test_pod_coverage -has_meta_yml ) ]
			    )
    };

plan( skip_all => 'Test::Kwalitee not installed; skipping' ) if $@;
