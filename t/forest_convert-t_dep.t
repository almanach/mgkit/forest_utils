#!/usr/bin/perl -w
use Test::More;
use File::Basename;
use strict;

# check if dot file contains no error

# compute plan
plan tests => 1;

# example: 'La Commission voit que les prix montent.'
my $dir = dirname($0);
my $output = `scripts/forest_convert.pl -f lp -t dep $dir/forest | dot -Tgif -otest.gif`;

is($output, '', "produce valid dot");
